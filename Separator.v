Set Implicit Arguments.
Set Contextual Implicit.

Require Import Utf8.
Require Import Setoid.
Require Import Morphisms.
Require Import Relation_Definitions.
Require Import TLC.LibLN.
Require Import Arith.
Require Import ImpAlg.Lattices.
Require Import ImpAlg.ImplicativeStructures.
Require Import Sets.Ensembles.
Require Import ImpAlg.Lambda.
Require Import ImpAlg.Adequacy.
Require Import ImpAlg.Combinators.
Require Import ImpAlg.ImplicativeAlgebras.


Section Separator.
  Context `{IS:ImplicativeStructure}.

  Definition upwards_closure (A:Ens):Ens:=
    fun (x:X) => exists (a:X), A a /\ (ord a x).
  Notation "↑ A":= (upwards_closure A) (at level 50).

  Inductive app_closure (A:Ens):Ens:=
  | is_base: forall a, A a -> app_closure A a
  | is_app:forall a, (exists b c, app_closure A b /\ app_closure A c /\ a=b@c) -> app_closure A a.                               

  Notation "[@ A ]":= (app_closure A) (at level 50).

  Definition induced_sep A:=↑([@ A]).


  Infix "@" := (app) (at level 59, left associativity).
  Notation "λ- f":= (abs f) (at level 60).

  Inductive KS:Ens:=
  |is_K: KS K
  |is_S: KS S
  .

  Hint Resolve is_base is_app is_S is_K.

  Definition Sep := induced_sep KS.

  Notation "a ∈ʆ":= (Sep a) (at level 80,no associativity).


  Lemma Sep_K : K ∈ʆ.
  Proof.
    exists K;intuition.
  Qed.

  Lemma Sep_S : S ∈ʆ.
  Proof.
    exists S;intuition.
  Qed.

  
  Lemma app_clos: is_app_closed Sep.
  Proof.
    intros a b Ha Hb.
    inversion Ha as (x,(Hx,Hax)).
    inversion Hb as (y,(Hy,Hby)). 
    exists (x@y);split;intuition.
    - apply is_app. exists x y;intuition.
    - apply app_mon;assumption.
  Qed.

  Lemma up_clos: is_upwards_closed Sep.
  Proof.
    intros a b Hb Ha.
    destruct Ha as (x,(Hx,Ha)).
    exists x ;intuition.
    transitivity a;assumption.
  Qed.

  
  Lemma mod_pon_clos:
    ∀ a b, a∈ʆ → (arrow a b)∈ʆ → b∈ʆ.
  Proof.
    apply mod_pon_app;[exact up_clos| exact app_clos].
  Qed.

  End Separator.

  Global Instance IS_IA `{IS:ImplicativeStructure}:ImplicativeAlgebra:=
    {separator:=Sep;
     modus_ponens:=mod_pon_clos;
     up_closed := up_clos;
     sep_K :=  Sep_K;
     sep_S := Sep_S 
    }.

  
      