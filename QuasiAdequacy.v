Set Implicit Arguments.
Set Contextual Implicit.

Require Import Utf8.
Require Import Setoid.
Require Import Morphisms.
Require Import Relation_Definitions.
Require Import TLC.LibLN.
Require Import Arith.
Require Import Lia.
Require Import Program.
Require Import Sets.Ensembles.
Require Import ImpAlg.Lattices.
Require Import ImpAlg.QuasiImplicativeStructures.
Require Import ImpAlg.Lambda.

(** The goal of this file is to prove the adequacy of the interpretation of λ-terms in quasi-implicative structures.
    Namely, writting [-] the intepretations of terms and types, we want to prove the following implication:
    
          ⊢ t: T         =>      [t] ≤ [T]  

   From now on, we assume that a quasi-implicative structure is fixed, we shall refer to it as IS. *)


Section Adequacy.
  Context `{QIS:QuasiImplicativeStructure}.


  Infix "@" := (oapp ) (at level 59, left associativity):ia_scope.
  Notation "λ- f":= (abs f) (at level 60):ia_scope.
  Definition cc `{QIS:QuasiImplicativeStructure} :=⋂[a] (⋂[b] (((a ↦ b) ↦ a) ↦ a)).
    
  Notation "{ k ~> u } t" := (open_rec_trm k t u) (at level 67).
  Notation "t ^^ u" := (open_trm t u) (at level 67).
  Notation "t ^c a" := (open_trm t (trm_cvar a)) (at level 67).
  Notation "t ^v a" := (open_trm t (trm_fvar a)) (at level 67).
  Notation "{ k ~t> U } T" := (open_rec_typ k T U) (at level 67).
  Notation "T ^t^ U" := (open_typ T U) (at level 67).
  Notation "T ^tc a" := (open_typ T (typ_cvar a)) (at level 67).
  Notation "T ^t X" := (open_typ T (typ_fvar X)) (at level 67).
      
(** * Proof tactics *)

Ltac gather_vars :=
    let A := gather_vars_with (fun x : vars => x) in
    let B := gather_vars_with (fun x : var => \{x}) in
    let C := gather_vars_with (fun x : env => (dom x)\u (@codom X x)) in
    let D := gather_vars_with (fun x : trm => @fv_trm X x) in
    let E := gather_vars_with (fun x : typ => @fv_typ X x) in
    constr:(A \u B \u C \u D \u E).

(** Tactic [pick_fresh x] adds to the context a new variable x
    and a proof that it is fresh from all of the other variables
    gathered by tactic [gather_vars]. *)

Ltac pick_fresh Y :=
  let L := gather_vars in (pick_fresh_gen L Y).

(** Tactic [apply_fresh T as y] takes a lemma T of the form 
    [forall L ..., (forall x, x \notin L, P x) -> ... -> Q.]
    instanciate L to be the set of variables occuring in the
    context (by [gather_vars]), then introduces for the premise
    with the cofinite quantification the name x as "y" (the second
    parameter of the tactic), and the proof that x is not in L. *)

Tactic Notation "apply_fresh" constr(T) "as" ident(x) :=
  apply_fresh_base T gather_vars x.
Tactic Notation "apply_fresh" "*" constr(T) "as" ident(x) :=
  apply_fresh T as x; auto_star.
Tactic Notation "apply_fresh" constr(T) :=
  apply_fresh_base T gather_vars ltac_no_arg.
Tactic Notation "apply_fresh" "*" constr(T) :=
  apply_fresh T; auto_star.

(** We define some tactics to automatize the resolution of goals
 involving simple inequalities and arbitrary meet *)

Ltac rewrite_S:=
  match goal with
    | H:_>=1 |- _ => let HS:= fresh H in destruct (geq_1_S H) as (?,HS);rewrite HS;simpl;try tauto
    | H:_>= S _ |- _ => let HS:= fresh H in destruct (geq_S_S H) as (?,(HS,?));rewrite HS;simpl;try tauto
    | H:_>= Datatypes.S _ |- _ => let HS:= fresh H in destruct (Datatypes.S H) as (?,(HS,?));rewrite HS;simpl;try tauto
  end.

Hint Resolve term_cvar term_var term_abs term_app.


(** * Embedding of λ-terms and types into implicative structures *)


(** We define a substitution as a function mapping variables 
  to elements of the implicative structure IS. *)

Definition substitution := var → X.

Definition update σ v c:var → X :=
  fun a => If v=a then c else σ a.


(** ** Translation of terms *)

Definition lift2 f (a b:option X):option X:=
  match a,b with
  | Some c,Some d => f c d
  | _,_ => None
  end.

(** Applications et flèches **partielles** *)

Notation "a o@ b":=(lift2 oapp a b) (at level 59).
Notation "a o↦ b":=(lift2 (fun x y => Some (x ↦ y)) a b) (at level 60, right associativity).


(** La fonction de traduction des termes, qui est définie comme une fonction partielle
des termes vers les élements de l'une structure quasi-implicative.
Le fioul sert juste de paramêtre de garantie de la bonne terminaison, 
en pratique on l'instancie avec la taille du terme puis l'on montre
que ça suffit pour tous les termes clos. *)


Fixpoint trans_trm_aux (σ:substitution) t fioul : option X :=
  match fioul with
  | 0 => None
  | Datatypes.S f =>
    match t with
    |trm_cvar x => Some x
    |trm_fvar x => Some (σ x)
    |trm_cc => Some cc
    |trm_app t1 t2 => (trans_trm_aux σ t1 f) o@ (trans_trm_aux σ t2 f)
    |trm_abs t' => Some (λ- (λ x,trans_trm_aux σ (t' ^c x) f))
    | _ => None
    end
  end.

Definition trans_trm σ t:= trans_trm_aux σ t (size t).




(** The function trans_trm is intrinsically limited by the size of the pre-term in argument, 
that is to say that if the amount of fioul is superior to the size of the argument, the result is unchanged. *)

Lemma trans_trm_fioul_aux:
  ∀ n p t σ, p<= n → size t <= p  → trans_trm_aux σ t p = trans_trm_aux σ t (size t).
Proof.
  intro n;induction n;intros p t σ Hp Size;[exfalso;apply (size_not_0 t);lia|].
  destruct t; destruct p;simpl in *;trivial;try lia.
  - f_equal. apply ord_antisym;apply meet_intro;intros z (y,(b,(Hb,Hy)));apply meet_elim;exists y;exists b;rewrite Hy;split;intuition;
      rewrite <- (size_open_c t y 0) in *;[ | symmetry ];f_equal;rewrite <- Hb; apply* IHn; lia.
  - assert (H1:=size_le_1 t1). assert (H2:=size_le_1 t2). lia.
  - assert (size t1 + size t2 <= S n) as H by lia.
    destruct (size_plus_S t1 t2) as (m,(Hm,(Hm1,Hm2))).
    rewrite Hm. simpl.    
    f_equal;symmetry; rewrite IHn;try lia;symmetry;apply IHn;lia.
Qed.

Lemma trans_trm_fioul:
  ∀ p t σ, size t <= p  → trans_trm_aux σ t p = trans_trm_aux σ t (size t).
Proof.
  intros p t σ Size; apply (@trans_trm_fioul_aux p);trivial.
Qed.

Lemma trans_trm_fold:
  ∀ t σ n, size t <= n → trans_trm_aux σ t n = trans_trm σ t.
Proof.
  intros t σ n Size.  unfold trans_trm.
  apply trans_trm_fioul;trivial.
Qed.



(** Properties of the translation function *)

(** Translation of application *)

Lemma trans_trm_app:
  ∀ t u σ, trans_trm σ (trm_app t u) = (trans_trm σ t) o@ (trans_trm σ u).
Proof.
  intros.
  unfold trans_trm.
  simpl.
  destruct (size_plus_S t u) as (p,(Hp,(Hp1,Hp2))).
  rewrite Hp. simpl.
  rewrite~ trans_trm_fioul.
  f_equal.
  rewrite~ trans_trm_fioul.
Qed.


(** Translation of abstraction *)

Lemma trans_trm_abs:
  ∀ t σ, trans_trm (σ:substitution) (trm_abs t) = Some (λ- (λ x, trans_trm σ (t ^c x))).
Proof.  
  intros.
  unfold trans_trm,open_trm.
  simpl. f_equal.
  apply ord_antisym.
  - apply meet_intro;intros x (y,Hy) ;apply meet_elim;exists y; rewrite~ (@size_open_c X) in Hy.
  - apply meet_intro;intros x (y,Hy) ;apply meet_elim;exists y; rewrite~ (@size_open_c X).
Qed.



(** Technical lemma on the translation of a term with a free variable
with respect to a substitution and the constant associated to this variable. *)

Lemma trans_trm_update_var_n:
  ∀ n t σ x a k, size t <= n → x \notin (fv_trm t) →
               trans_trm (update σ x a) ({k ~> trm_fvar x} t) = trans_trm σ ({k ~> trm_cvar a} t).
Proof.
  intro n;induction n;intros. exfalso. apply (size_not_0 _ H).
  destruct t;simpl;intros;auto.
  - case_nat; cbn.
    + f_equal. unfold update. now rewrite If_l.
    + now cbn.
  - cbn. f_equal. apply If_r. apply (notin_singleton_r H0).
  - unfold open_trm.  simpl. do 2 rewrite trans_trm_abs.
    f_equal. apply meet_same_set;split;
               intros y [z [b [Hb Hx]]]; exists z;exists b;split;auto;rewrite <- Hb; [symmetry|]; unfold open_trm; do 2 (rewrite open_trm_com_ct;auto);
      apply IHn ;unfold open_trm;
      [rewrite size_open_c;simpl in H;try lia|rewrite fv_trm_subst;now simpl in H0
      |rewrite size_open_c;simpl in H;try lia|rewrite fv_trm_subst;now simpl in H0].
  - do 2 rewrite trans_trm_app;f_equal;
    rewrite IHn;auto;
      [simpl in H;apply size_app_le_l in H;try lia|simpl in H0;apply notin_union in H0 as [H1 H2];auto
      |simpl in H;apply size_app_le_r in H;try lia|simpl in H0;apply notin_union in H0 as [H1 H2];auto].
Qed.

Lemma trans_trm_update_var:
  ∀ t σ x a k, x \notin (fv_trm t) →
               trans_trm (update σ x a) ({k ~> trm_fvar x} t) = trans_trm σ ({k ~> trm_cvar a} t).
Proof.
  intros t σ c y k H. now rewrite (@trans_trm_update_var_n (size t)).
Qed.
 

(** ** Translation of types *)

(** We apply the same rationale to define the translation 
of types through a partial function. Observe that in particular,
any element of the QIS (seen as a constant type) is translated into itself. *)


Inductive translated_typ : typ → option X → Prop :=
|tr_typ_var: ∀ x, translated_typ (typ_cvar x) (Some x)
|tr_typ_arrow: ∀ T U x y, (translated_typ T x)
                          → (translated_typ U y)
                          → translated_typ (typ_arrow T U) (x o↦ y)
|tr_typ_fall: ∀ T f, (∀ x, translated_typ (T ^tc x) (f x))
                     → translated_typ (typ_fall T) (Some(⊓ (fun x => ∃ y, Some x=f y))).


Fixpoint trans_typ_aux (σ:substitution) (T: typ) fioul {struct fioul}: option X :=
  match fioul with
    | 0 => None
    | Datatypes.S f =>
      (match T with
         |typ_cvar x => Some x
         |typ_fvar x => Some (σ x)
         |typ_arrow T U => (trans_typ_aux σ T (f)) o↦ (trans_typ_aux σ U (f))
         |typ_fall T => Some (⊓ (fun x => ∃ y, Some x=trans_typ_aux σ (T ^tc y) (f)))
         | _ => None
       end)
  end.


Definition trans_typ σ T:=trans_typ_aux σ T (size_typ T).


(** Translation of the ∀ *)

Lemma trans_typ_fall: ∀ T σ,
         trans_typ (σ:substitution) (typ_fall T) = Some (⊓ (fun x => ∃ y, Some x=trans_typ σ (T ^tc y))).
Proof.
  intros.
  unfold trans_typ,open_typ.
  simpl;f_equal.
  apply ord_antisym.
  - apply meet_intro;intros x (y,Hy) ;apply meet_elim;exists y; rewrite~ (@size_typ_open X) in Hy.
  - apply meet_intro;intros x (y,Hy) ;apply meet_elim;exists y; rewrite~ (@size_typ_open X).
Qed.



(** The function trans_typ is intrinsically limited by the size of the pre-type in argument, 
that is to say that if the amount of fioul is superior to the size of the argument, the result is unchanged. *)

Lemma trans_typ_fioul: 
  ∀ T σ n, size_typ T <= n → trans_typ_aux σ T n = trans_typ_aux σ T (size_typ T).
Proof.
  apply (size_typ_ind (fun T =>  ∀ (σ : substitution) (n : nat),
   n ≥ size_typ T →  trans_typ_aux σ T n = trans_typ_aux σ T (size_typ T))).  intros n IH T Size σ m Hm.  
  - destruct T; simpl in *;try rewrite_S.
    + f_equal. apply eq_add_S in Size. apply ord_antisym;apply meet_intro;intros z (y,Hy) ;apply meet_elim;exists y;rewrite Hy;
      rewrite <- (size_typ_open T y 0) in *;[symmetry  | ];apply* IH;rewrite~ Size.
    + destruct (size_typ_plus_S T1 T2) as (p,(Hp,(Hp1,Hp2))).
      rewrite Hp in *.
      destruct m;try lia.
      inversion Size;subst.
      inversion Hm;subst;trivial. simpl.
      rewrite~ (IH T1);[|lia].
      rewrite~ (IH T2);[|lia].
      f_equal;symmetry;apply* IH.
Qed.

Lemma trans_typ_fold: 
  ∀ T σ n, size_typ T <= n → trans_typ_aux σ T n = trans_typ σ T.
Proof.
  intros. unfold trans_typ.
  apply~ trans_typ_fioul.
Qed.
  
Lemma trans_typ_arrow: ∀ T U σ,
           trans_typ σ (typ_arrow T U) = (trans_typ σ T) o↦ (trans_typ σ U).
Proof.
  intros.  unfold trans_typ.  simpl.
  destruct (size_typ_plus_S T U) as (p,(Hp,(Hp1,Hp2))).
  rewrite Hp.  simpl.
  rewrite~ trans_typ_fioul.
  f_equal.
  rewrite~ trans_typ_fioul.
Qed.


Lemma fv_typ_subst: ∀ (T:@typ X) y, fv_typ ({0 ~t> typ_cvar y} T) = fv_typ T.
Proof.
  intro T.
  generalize 0.
  induction T;simpl;intros;auto.
  - case_nat;simpl;auto.
  - rewrite IHT1. rewrite~ IHT2.
Qed.







(** We say that a substitution σ "realize" an environement Γ 
if for each hypothesis (a:A) ∈ Γ, σ(a) = [A]^σ :*)

Inductive realize : substitution → env → Prop:=
|real_empty : ∀ σ, realize σ empty
|real_push : ∀ σ a A Γ, Some (σ a) ≤o trans_typ σ A →  realize σ Γ → realize σ (Γ & (single a A))
.

Infix "⊨":= realize (at level 59).

(** 
  We prove a few properties about the previous definition:
 *)
  
Lemma realize_inv:
  ∀ σ x T Γ a, binds x T Γ → realize σ Γ
               →  trans_typ σ T=Some a → σ x ≤ a.
Proof.
  intros σ x T Γ.
  induction Γ;intros.
  - contradict H.
    rewrite <- empty_def .
    intro.
    apply (binds_empty_inv H).
  - destruct a as (v,A).
    inversion H0.
    + rewrite <- cons_to_push in H2.
      inversion H2.
      subst v;subst A0;subst Γ0.
      clear H2.
      rewrite cons_to_push in *.
      destruct (@var_eq_dec x a) as [Hx | Hx].
      * subst. apply binds_push_eq_inv in H.
        subst. inversion H3. rewrite H1 in H2.
        inversion H2;now subst.
      * apply IHΓ;try assumption.
        apply (binds_push_neq_inv H Hx).
Qed.

Lemma trans_typ_update_n:
  ∀ n U σ c y, size_typ U <= n → c \notin (fv_typ U) → trans_typ (update σ c y) U =trans_typ σ U.
Proof.
  intro n;induction n;intros. exfalso. apply (size_typ_not_0 _ H).
  destruct U;simpl;intros;auto.
  - cbn. f_equal. apply If_r. apply (notin_singleton_r H0).
  - do 2 rewrite trans_typ_fall.
    f_equal. apply meet_same_set;split;
    intros x [z Hx]; exists z;rewrite Hx; [ |symmetry];apply IHn;unfold open_typ;
      [rewrite size_typ_open;simpl in H;try lia|rewrite fv_typ_subst;now simpl in H0
      |rewrite size_typ_open;simpl in H;try lia|rewrite fv_typ_subst;now simpl in H0].
  - do 2 rewrite trans_typ_arrow;f_equal;
    rewrite IHn;auto;
      [simpl in H;apply size_typ_arrow_le_l in H;try lia|simpl in H0;apply notin_union in H0 as [H1 H2];auto
      |simpl in H;apply size_typ_arrow_le_r in H;try lia|simpl in H0;apply notin_union in H0 as [H1 H2];auto].
Qed.

Lemma trans_typ_update:
  ∀ U σ c y, c \notin (fv_typ U) → trans_typ (update σ c y) U =trans_typ σ U.
Proof.
  intros U σ c y H. now rewrite (@trans_typ_update_n (size_typ U)).
Qed.

Lemma realize_update:
  ∀ Γ σ c y, c \notin ((dom Γ)\u (codom Γ)) → realize σ Γ → realize (update σ c y) Γ.
Proof.
  intro Γ.
  induction Γ;intros.
  - apply real_empty.
  - destruct a as (v,A).
    rewrite cons_to_push in *.
    inversion H0.
    + apply real_empty.
    + repeat (rewrite <- cons_to_push in H1).
      inversion H1.
      subst. assert (H':=H).
      apply (@real_push _ _ _ _ ).
      rewrite dom_push in H. 
      do 2 (apply  notin_union_r1 in H).
      rewrite notin_singleton in H.
      unfold update at 1. rewrite If_r;auto.
      rewrite trans_typ_update;auto.
      apply notin_union_r2 in H';
        rewrite codom_push in H';
        now apply notin_union_r1 in H'.
      apply IHΓ;auto. rewrite dom_push,codom_push in H;auto.
Qed.



Inductive included : (@env X)→ (@env X)→ Prop:=
| incl_empty: ∀ Γ', included empty Γ'
| incl_push: ∀ Γ Γ' a A, included Γ Γ' → included (concat Γ (single a  A)) (concat Γ' (single a  A))
| incl_prefix: ∀ Γ Γ' a A, included Γ Γ' → included Γ (concat Γ' (single a  A)).

Infix "⊆":=included (at level 59).

Lemma realize_included:  ∀ Γ Γ' σ,  Γ ⊆ Γ' → realize σ  Γ' →  realize σ Γ.
Proof.
  intros Γ Γ' σ Weak real.
  induction Weak.
  - apply real_empty.
  - inversion real.
    + exfalso.
      apply binds_empty_inv with typ a A.
      rewrite empty_eq,H1.
      apply binds_push_eq.
    + apply eq_push_inv in H as (bli,(bla,blo));subst. 
      inversion real.
      * exfalso. apply binds_empty_inv with typ a A.
      rewrite empty_eq,H3. apply binds_push_eq.
      * apply eq_push_inv in H as (Ha,(HA,HG));subst. 
        apply real_push;auto.
  - inversion real.
    + exfalso.
      apply binds_empty_inv with typ a A.
      rewrite empty_eq,H1.
      apply binds_push_eq.
    + apply eq_push_inv in H as (bli,(bla,blo));subst.
      auto.
Qed.
  


(** The translation of types is well-defined. *)


Lemma trans_type_some_n: ∀ n T σ, size_typ T <= n → type T → ∃ a, trans_typ σ T = Some a.
Proof.
  intros n;induction n;intros T σ Size Typ;
    [exfalso; apply (size_typ_not_0 _ Size) | ].
  destruct T;try (now inversion Typ);try (cbn;eexists;f_equal;easy).
  cbn. inversion Typ;subst.
  destruct (size_typ_plus_S T1 T2) as [m [Hm [S1 S2]]].
  rewrite Hm;cbn.
  simpl in Size. 
  assert (m<=n) by lia.
  destruct (IHn T1 σ);try lia;auto.
  destruct (IHn T2 σ);try lia;auto.
  exists (x ↦ x0).
  unfold trans_typ in H0,H3.
  rewrite <- (@trans_typ_fioul _ _ m) in H0 by lia.
  rewrite <- (@trans_typ_fioul _ _ m) in H3 by lia.
  rewrite H0,H3;easy.
Qed.  

Lemma trans_type_some: ∀ T σ, type T → ∃ a, trans_typ σ T = Some a.
Proof.
  intros T σ Typ. now apply (@trans_type_some_n (size_typ T)).  
Qed.  


Lemma trans_typ_arrow_some: ∀ T U σ a, trans_typ σ (typ_arrow T U) = Some a →
              ∃ x y, (a = x ↦ y /\ trans_typ σ T = Some x  /\ trans_typ σ U = Some y). 
Proof.
  intros T U σ a Ha.
  cbn in Ha.
  destruct (size_typ_plus_S T U) as [m [Hm [ST SU]]].
  rewrite Hm in Ha;cbn in Ha.
  do 2 rewrite (@trans_typ_fioul _ _ m) in Ha by lia.
  do 2 rewrite trans_typ_fold in Ha by lia.
  unfold lift2 in Ha.
  destruct (@option_dec _ (trans_typ σ T)) as [HT|[aT HT]];
    rewrite HT in Ha; inversion Ha.
  destruct (@option_dec _ (trans_typ σ U)) as [HU|[aU HU]];
    rewrite HU in Ha; inversion Ha.
  rewrite HU,HT;do 2 eexists;repeat split;easy.
Qed.





(*********************************************)
(** *   Adequacy of semantic typing rules    *)
(*********************************************)

(** On définit le séquent  [ Γ ⊢ t $ T ] par:
 pour toute substitution σ, si σ réalise Γ et que la traduction de T est bien 
définie (ce qui est le cas si T est un type, et en particulier pour tous les
élements de la structure quasiimplicative), alors la traduction de t est bien 
définie et est inférieure à celle de T. *)
  
Definition valid Γ t T:=  
∀ σ a, σ ⊨ Γ →  (trans_typ σ T = Some a) → ∃x,(trans_trm σ t = Some x) /\ (x ≤ a).

Notation "[ Γ ⊢ t $ T ]" := (valid Γ t T).



(**

Axiome :

   (x:T) ∈ Γ 
  ------------
   Γ ⊢ x : T

*)

Lemma axiome:
  ∀ Γ x T, binds x T Γ → [ Γ ⊢ trm_fvar x $ T ].
Proof.
  intros Γ x T Binds σ a real HT.
  apply (realize_inv Binds real) in HT.
  exists (σ x);split;auto.
Qed.




(** 

Introduction de la flèche:

    Γ,x: T ⊢ t:U 
  ----------------
  Γ ⊢ λx.t : T→ U

*)

Lemma imp_intro: ∀ L Γ t T U, (∀ x, x \notin L →
    [ (concat Γ (single x T)) ⊢ (t ^v x) $ U ])  → [ Γ ⊢ trm_abs t $ typ_arrow T U ].
Proof.
  intros L0 Γ t T U H σ a Real Typ.
  apply trans_typ_arrow_some in Typ as [aT [aU [Ha [HT HU]]]].
  subst.
  pick_fresh x.
  assert (x \notin L0);auto.
  destruct (H x H0 (update σ x aT) aU) as (z,(Ht,Hz)).
  - apply real_push;auto.
    + rewrite trans_typ_update;auto.
      rewrite HT. apply ord_some,by_refl.
      unfold update;now rewrite If_l.
    + apply realize_update;auto.
  - rewrite trans_typ_update;auto.
  - cbn;eexists;split;try easy.
    apply ord_trans with (aT ↦ z);[|now apply arrow_mon_r].
    apply meet_elim.
   exists aT; exists z;split;auto.
    rewrite <- Ht,trans_trm_fold;[|unfold open_trm;now rewrite size_open_c].
    symmetry. apply trans_trm_update_var. auto.
Qed.  



(** 

Élimination de la flèche:

    Γ ⊢ t:T→U   Γ ⊢ u:U 
  ---------------------
         Γ ⊢ t u : U

(où l'on suppose que T est un type)
*)



Lemma imp_elim:
  ∀ Γ t u T U, type T → [ Γ ⊢ t $ typ_arrow T U ] → [ Γ ⊢ u $ T ] → [ Γ ⊢ trm_app t u $ U].
Proof.
  intros Γ t u T U HT Ht Hu σ a Real HU.
  apply (trans_type_some σ) in HT as [aT HT].
  cbn.
  destruct (size_plus_S t u) as [n [Stu [St Su]]].
  rewrite Stu. cbn.
  do 2 (rewrite trans_trm_fioul;[rewrite trans_trm_fold|];auto).
  specialize (Hu σ aT Real HT) as [ua [Hu Hua]].
  specialize (Ht σ (aT ↦ a) Real) as [ta [Ht Hta]].
  - cbn.
    destruct (size_typ_plus_S T U) as [m [STU [ST SU]]].
    rewrite STU. cbn.
    do 2 (rewrite trans_typ_fioul;[rewrite trans_typ_fold|];auto).
    now rewrite HT,HU.
  - rewrite Ht,Hu. eexists;split.
    unfold oapp,omeet_set,lift2, QuasiImplicativeStructures.U.
    + cbn. rewrite If_l;auto.
      unfold QuasiImplicativeStructures.U.
      exists a. apply ord_trans with (aT ↦ a);auto.
      now apply arrow_mon_l.
    + apply meet_elim.
      apply ord_trans with (aT ↦ a);auto.
      now apply arrow_mon_l.
Qed.




(**

Introduction du ∀:

   Γ ⊢ t : T[X:= c]  
  -------------------(∀c)
    Γ ⊢ t:∀X.T 

(où l'on suppose que ∀X.T est un type)
*)



Lemma fall_intro:
  ∀ Γ t T, type (typ_fall T) → (∀ c, [ Γ ⊢ t $ T ^tc c ]) → [ Γ ⊢ t $ typ_fall T ].
Proof.
  intros Γ t T HT H σ a real Typ.
  cbn in Typ. inversion Typ;subst;clear Typ.
  assert (H':=H).
  inversion HT.
  pick_fresh v.
  repeat (apply notin_union_r1 in Fr).
  specialize (H1 v Fr).
  rewrite (type_subst T v ⊤) in H1.
  apply (trans_type_some σ) in H1.
  destruct H1 as [xT HxT].
  destruct (H ⊤ σ xT real HxT) as [xt [Ht Hx]];auto.
  exists xt;split;auto.
  apply meet_intro.
  intros x [y Hy].
  specialize (H' y σ x real) as [z [Htz Hz]].
  - rewrite trans_typ_fioul,trans_typ_fold in Hy;auto.
    unfold open_typ;now rewrite size_typ_open.
  - rewrite Ht in Htz. now inversion Htz.
Qed.


(**

Élimination du ∀:

    Γ ⊢ t:∀X.T 
  ---------------------
   Γ ⊢ t : T[X:= c]

*)

Lemma fall_elim:
  ∀ Γ t  T c,  [ Γ ⊢ t $ typ_fall T ] → [ Γ ⊢ t $ T ^tc c ].
Proof.
  intros Γ t T c H σ a real Typ.
  pose (x:=trans_typ σ (typ_fall T)).
  cbn in x.
  specialize (H σ ((⊓ (λ x : X, exists y, Some x = trans_typ_aux σ (T ^tc y) (size_typ T)))) real) as [xt [Ht Hx]].
  - now cbn.
  - exists xt;split;auto.
    apply ord_trans with (⊓ (λ x : X, exists y, Some x = trans_typ_aux σ (T ^tc y) (size_typ T)));auto.
    apply meet_elim. exists c. symmetry. unfold trans_typ,open_typ in Typ.
    rewrite size_typ_open in Typ.
    apply Typ.
Qed.



(**

Affaiblissiement du contexte:

    Γ ⊢ t:T    Γ ⊆ Γ'
  ---------------------
      Γ' ⊢ t : T

*)



Lemma weakening:
  ∀ Γ Γ' t T,  Γ ⊆ Γ' → [ Γ ⊢ t $ T ] → [ Γ' ⊢ t $ T ].
Proof.
  intros Γ Γ' t T Weak H σ a real HT.
  destruct (H σ a) as [x [Ht Hx]];auto.  
  - now apply realize_included with Γ'.
  - exists x;split;auto.
Qed.




(**

Subsumption :

   Γ ⊢ t : T   T ≤ T'
  -------------------
      Γ ⊢ t : T'

(où l'on se restreint au cas de T,T' élements 
de la structure quasi-implicative par simplicité)

*)

Lemma subsumption:
  ∀ Γ t a a', a ≤ a' → [ Γ ⊢ t $ typ_cvar a ] → [ Γ ⊢ t $ typ_cvar a' ].
Proof.
  intros Γ t a a' Ha H σ b real HT.
  cbn in HT;inversion HT;subst.
  destruct (H σ a) as [x [Ht Hx]];auto.  
  exists x;split;auto.
  now apply ord_trans with a.
Qed.





End Adequacy.
