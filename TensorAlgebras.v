Set Implicit Arguments.
Set Contextual Implicit.

Require Import Utf8.
Require Import Setoid.
Require Import Sets.Ensembles.
Require Import ImpAlg.Lattices.
Require Export ImpAlg.ImplicativeStructures.
Require Export ImpAlg.ParAlgebras.
(*Require Export ImpAlg.Translations.*)


Ltac auto_meet_intro_elim:= repeat auto_meet_intro;auto_meet_elim_trans.


(** This file contains the definitions relative
to the notion of conjunctive algebras, as well as
various properties about it *) 


(** * Conjunctive Structures *)


Definition tensor_set_l `{X:Type} `{tensor:X → X → X} (A:Ens) (b:X):Ens :=
    fun x => ∃a, A a ∧ x = tensor a b.
Definition tensor_set_r `{X:Type} `{tensor:X → X → X} (a:X) (B:Ens):Ens :=
    fun x => ∃b, B b ∧ x = tensor a b.
Definition tensor_set `{X:Type} `{tensor:X → X → X} (A:Ens) (B:Ens):Ens :=
    fun x => ∃a,∃b, A a ∧ B b ∧ x = tensor a b.
Notation "a ◃ b":= (rev a b) (at level 70).


(** A conjunctive structure (A,≤,¬,⊗) is a complete lattice equipped with 
two internal laws ¬ and ⊗ such that:
- a ≤ a' ⇒  b ≤ b'  ⇒  a ⊗ b ≤ a'⊗ b'  
- a ≤ a' ⇒  ¬ a' ≤ ¬ a 
- ⋃[a] (a ⊗ b) = (⋃[a] a) ⊗ b   and      ⋃[b] (a ⊗ b) =  a ⊗ (⋃[b] b)  
- ¬ ⋃ {a : a∈A} = ⋂ {¬ a: a∈ A}
*)



Class TensorStructure `{CL:CompleteLattice} :=
  {
    tensor:X → X → X;
    tneg: X → X;
    
    tensor_mon: ∀ a a' b b',  a ≤ a' -> b ≤ b' -> (tensor a b ) ≤ (tensor a' b');

    tneg_mon: ∀ a a', ord a a' ->  ord (tneg a') (tneg a);
   
    tensor_join_l: ∀ A b, tensor (join_set A) b = join_set (@tensor_set_l X tensor A b);
    tensor_join_r: ∀ a B, tensor a (join_set B) = join_set (@tensor_set_r X tensor a B);
    
    tneg_join: ∀ A, tneg (join_set A) = meet_set (image_set tneg A)
  }.

Generalizable Variables X ord join meet tneg par L meet_set join_set.
Infix " ⊗ ":= tensor (at level 76, right associativity):ta_scope.
Definition tarrow  `{TS:TensorStructure} a b:= (tneg( tensor a (tneg b))).
Notation "a ⊗↦ b":=  (tarrow a b) (at level 60, right associativity):ta_scope.
Global Open Scope ta_scope.




(** ** Properties *)

Section About_TensorStruct.
Context `{TS:TensorStructure}.

 (** *** Properties of the ⊗ operator *)

  Lemma tensor_mon_l: ∀ a a' b, a ≤ a' ->  (a ⊗ b) ≤ (a' ⊗ b).
  Proof.
  intros a a' b H.
  apply tensor_mon;intuition.
  Qed.
  
  Lemma tensor_mon_r: ∀ a b b', b ≤ b' ->  (a ⊗ b) ≤ (a ⊗ b').
  Proof.
  intros a b b' H.
  apply tensor_mon;intuition.
  Qed.

  Lemma tensor_bot_l: ∀ a:X, (⊥ ⊗ a) = ⊥. 
  Proof.
    intros a.
    rewrite <- join_emptyset.
    rewrite tensor_join_l.
    apply join_same_set.
  split;[intros x (y,(H,_))| intros x H];contradict H.
  Qed.
  
  Lemma tensor_bot_r: ∀ a:X, (a ⊗ ⊥) = ⊥. 
  Proof.
    intros a.
    rewrite <- join_emptyset.
    rewrite tensor_join_r.
    apply join_same_set.
    split;[intros x (y,(H,_))| intros x H];contradict H.
  Qed.
  
  
  (** *** Properties of the induced arrow *)

  (** Variance *)
  
  Lemma tarrow_mon: ∀ a a' b b', a'≤ a → b ≤ b' ->  (a ⊗↦ b) ≤ (a' ⊗↦ b').
  Proof.
    intros a a' b b' Ha Hb.
    apply tneg_mon.
    apply tensor_mon;intuition.
    apply tneg_mon;intuition.
  Qed.

  Lemma tarrow_mon_r: ∀ a b b', b ≤ b' ->  (a ⊗↦ b) ≤ (a ⊗↦ b').
  Proof.
    intros;apply tarrow_mon;intuition.
  Qed.
    
  Lemma tarrow_mon_l: ∀ a a' b, a≤ a' ->  (a' ⊗↦ b) ≤ (a ⊗↦ b).
  Proof.
    intros;apply tarrow_mon;intuition.
  Qed.


  (** Distributivity *)
  
  Lemma tarrow_join : ∀ A b, (join_set A) ⊗↦ b = meet_set (λ x, ∃a, A a ∧ x=a⊗↦b).
  Proof.
    intros;unfold tarrow.
    rewrite tensor_join_l.
    rewrite tneg_join.
    apply meet_same_set;split;unfold Included,In,image_set,tensor_set_l;intros x (a,H).
    - repeat destruct H;subst. exists x0;intuition.
    - destruct H. eexists. split;[|exact H0]. exists a;intuition. 
  Qed.
  
    

  (** *** Properties of the ¬ operator *)
  
  Lemma tneg_bot: (tneg ⊥) = ⊤. 
  Proof.
    rewrite <- join_emptyset.
    rewrite tneg_join.
    rewrite <- meet_emptyset.
    apply meet_same_set.
    split;[intros x (y,(H,_))| intros x H];contradict H.
  Qed.

  Lemma tneg_sup (f:X→X):  tneg (⋃[a] (f a)) = ⋂[a] (tneg (f a)).
  Proof.
    rewrite tneg_join.
    apply meet_same_set.
    split;intros x (a,Ha).
    - destruct Ha as ((b,Hb),Hx); subst;eexists;try assumption;reflexivity.
    - exists (f a);split;[later|];try assumption;reflexivity.
  Qed.
  
End About_TensorStruct.


Hint Resolve tensor_mon tneg_mon tensor_join_l tensor_join_r tneg_join.



(** ** Reversing conjunctive/disjunctive structures *)

(** ***    From  ⅋-structures to ⊗-structures     *)

(** We show that starting from a disjunctive structure,
the structure based on the reversed underlying complete lattice 
with the same negation and a⊗b := a⅋b  defines a conjunctive structure. *)

Section ReversePS.
  Context `{PS:ParStructure}.

  
  Definition tens a b:=par a b.
  
  Ltac autorev:=intros;unfold tens,rev,reverse;auto.
  
 
 Lemma rev_tensor_mon: ∀ a a' b b', a ◃ a' -> b ◃ b' ->  tens a  b ◃ tens a'  b'.
  Proof.
    autorev.
  Qed.
  
  Lemma rev_tneg_mon: ∀ a a', a ◃ a' -> (¬ a') ◃ (¬ a).
  Proof.
    autorev.
  Qed.

  Lemma rev_tensor_join_l: ∀ A b, tens (meet_set A) b = meet_set (@tensor_set_l X tens A b).
  Proof.
    unfold tensor_set_l.
    apply par_meet_l.
  Qed.
  
  Lemma rev_tensor_join_r: ∀ a B, tens a (meet_set B) = meet_set (@tensor_set_r X tens a B).
  Proof.
    unfold tensor_set_l.
    apply par_meet_r.
  Qed.

  Lemma rev_tneg_join: ∀ A, pneg (meet_set A) = join_set (image_set pneg A).
  Proof.
    apply pneg_meet.
  Qed.  
End ReversePS.


Global Instance PS_TS `{PS:ParStructure}:@TensorStructure _ _ _ RevCompleteLattice:=
  {tensor:=par; tneg:= pneg; tensor_mon:=rev_tensor_mon;
   tneg_mon:=rev_tneg_mon;tensor_join_l:=rev_tensor_join_l;
   tensor_join_r:=rev_tensor_join_r; tneg_join:=rev_tneg_join}.



(** ***    From ⊗-structures to ⅋-structures       *)

(** Conversly, we show that starting from a conjunctive structure,
the structure based on the reversed underlying complete lattice 
with the same negation and a⅋b := a⊗b  defines a disjunctive structure. *)


Section ReverseTS.
  Context `{TS:TensorStructure}.

  Notation "¬ a":=(tneg a).
  
  Ltac autorev:=intros;unfold rev,reverse;auto.

  Lemma rev_par_mon: ∀ a a' b b', a ◃ a' -> b ◃ b' -> tensor a  b ◃ tensor a'  b'.
  Proof.
    autorev.
  Qed.
    
  Lemma rev_pneg_mon: ∀ a a', a ◃ a' -> (¬ a') ◃ (¬ a).
  Proof.
    autorev.
  Qed.

  Lemma rev_par_meet_l: ∀ A b, tensor (join_set A) b = join_set (@par_set_l X tensor A b).
  Proof.
    unfold par_set_l. 
    apply tensor_join_l.
  Qed.
  
  Lemma rev_par_meet_r: ∀ a B, tensor a  (join_set B) = join_set (@par_set_r X tensor a B).
  Proof.
    unfold par_set_l. 
    apply tensor_join_r.
  Qed.

  Lemma rev_pneg_meet: ∀ A, tneg (join_set A) = meet_set (image_set tneg A).
  Proof.
    apply tneg_join.
  Qed.

End ReverseTS.


Global Instance TS_PS `{TS:TensorStructure}:@ParStructure _ _ _ RevCompleteLattice:=
  {par:=tensor; pneg:=tneg; par_mon:=rev_par_mon;
   pneg_mon:=rev_pneg_mon; par_meet_l:=rev_par_meet_l; par_meet_r:=rev_par_meet_r;
   pneg_meet:=rev_pneg_meet}.




(** * Conjunctive algebras *)

(** We define conjunctive algebras as the given of a conjunctive structure
together with a separator which contains at least the following combinators. *)

Notation "¬ a":=(tneg a).
Definition TS1 `{TS:TensorStructure}:= ⋂[ a ] ¬ (¬(a ⊗ a)⊗a).
Definition TS2 `{TS:TensorStructure}:= ⋂[ a ] (⋂[ b ] ¬(¬ a ⊗ (a ⊗ b))). 
Definition TS3 `{TS:TensorStructure}:= ⋂[ a ] (⋂[ b ] ¬(¬(a ⊗ b) ⊗ (b ⊗ a))).
Definition TS4 `{TS:TensorStructure}:= ⋂[ a ] ⋂[ b ] ⋂[ c ] ¬(¬(¬a ⊗ b) ⊗ ¬(c ⊗ a) ⊗ (c ⊗ b)).
Definition TS5 `{TS:TensorStructure}:= ⋂[ a ] ⋂[ b ] ⋂[ c ] ¬(¬(a ⊗ (b ⊗ c)) ⊗ ((a ⊗ b) ⊗ c)).


Class TensorAlgebra `{TS:TensorStructure}:=
  {
    tsep: X → Prop;
    tmodus_ponens: ∀ a b, tsep a → tsep (¬(a ⊗ b)) → tsep (¬b);
    tpair: ∀ a b, tsep a → tsep b → tsep (a ⊗ b);
    tup_closed: ∀ a b, ord a b → tsep a → tsep b;
    tsep_TS1 : tsep TS1;
    tsep_TS2 : tsep TS2;
    tsep_TS3 : tsep TS3;
    tsep_TS4 : tsep TS4;
    tsep_TS5 : tsep TS5;
  }.

(** Classical conjunctive algebras (ktdne is the extra axiom) *)

Class KTensorAlgebra `{TS:TensorStructure}:=
  {
    TA:> TensorAlgebra;
    tdne: ∀ a , tsep (¬¬ a) → tsep a;
  }.



(** Explosive conjunctive algebras (xplosive is the extra axiom) *)

Class XTensorAlgebra `{TS:TensorStructure}:=
  {
    xtsep: X → Prop;
    xtmodus_ponens: ∀ a b, xtsep a → xtsep (¬(a ⊗ b)) → xtsep (¬b);
    xplosive: ∀ a , xtsep (¬ a) → xtsep a → xtsep ⊥;
    xtpair: ∀ a b, xtsep a → xtsep b → xtsep (a ⊗ b);
    xtup_closed: ∀ a b, ord a b → xtsep a → xtsep b;
    xtsep_TS1 : xtsep TS1;
    xtsep_TS2 : xtsep TS2;
    xtsep_TS3 : xtsep TS3;
    xtsep_TS4 : xtsep TS4;
    xtsep_TS5 : xtsep TS5;
  }.


(* Hint Resolve ktsep_TS1  ktsep_TS2 ktsep_TS3 ktsep_TS4 ktsep_TS5. *)
Hint Resolve tsep_TS1  tsep_TS2 tsep_TS3 tsep_TS4 tsep_TS5.


(** Classical are explosive, explosive are usual *)

(* Section KTA. *)
(*   Context `{KTA:KTensorAlgebra}. *)
(*   Global Instance KTA_TA :TensorAlgebra:= *)
(*   {tsep:=ktsep;tpair:=ktpair;tmodus_ponens:=ktmodus_ponens; *)
(*    tup_closed:= ktup_closed; tsep_TS1:=ktsep_TS1; *)
(*    tsep_TS2:=ktsep_TS2;tsep_TS3:=ktsep_TS3;tsep_TS4:=ktsep_TS4;tsep_TS5:=ktsep_TS5}. *)
(* End KTA. *)

Section XTA.
  Context `{XTA:XTensorAlgebra}.
  Global Instance XTA_TA :TensorAlgebra:=
  {tsep:=xtsep;tpair:=xtpair;tmodus_ponens:=xtmodus_ponens;
   tup_closed:= xtup_closed; tsep_TS1:=xtsep_TS1;
   tsep_TS2:=xtsep_TS2;tsep_TS3:=xtsep_TS3;tsep_TS4:=xtsep_TS4;tsep_TS5:=xtsep_TS5}.
End XTA.


(* Coercion KTA_TA: KTensorAlgebra >-> TensorAlgebra. *)
Coercion XTA_TA: XTensorAlgebra >-> TensorAlgebra.

Notation "a '∈ʆ'":= (tsep a) (at level 80,no associativity):ta_scope.
Global Open Scope ta_scope.
Hint Resolve tsep_TS1 tsep_TS2 tsep_TS3 tsep_TS4 tsep_TS5. 



(** ** Internal logic *)

Section About_TensorAlg.
Context `{TA:TensorAlgebra}.
Notation "¬ a":=(tneg a).
Definition ts_abs t:= ⊓(λ x:X, ∃ a:X, x= ⊓ (λ y : X, ∃ b :X, y = (¬(a ⊗ ¬b)) ∧ t a ≤ b)).


(** *** Entailment relations *)

(** We define the usual entailement relation, for which the (meta) 
    modus-ponens is not valid in general *)


Definition entails a b := tsep (a⊗↦b). 
Notation "a ⊢⊗ b" := (entails a b) (at level 75). 


(** Alternative (negative) entailement relation, adapted to 
    the deduction rule  a∈ʆ ⇒  ¬(a⊗b)∈ʆ   ⇒ ¬b∈ʆ *)


Definition nentails a b := tsep (¬(a⊗b)).
Notation "a ⊢ ¬ b" := (nentails a b) (at level 55).

Notation "a '[i]⊢¬' b":= ((⋂[i]¬(a i ⊗ b i)) ∈ʆ) (at level 59).
Notation "a '[i]⊢⊗' b":= ((⋂[i](a i ⊗↦ b i)) ∈ʆ) (at level 59).
Notation "'¬i' a" := (λ i, ¬ ( a i)) (at level 59).


(** A few technical lemmas to simplify further proofs *)

Ltac auto_sep_Si ax:=
  intros;
  apply (tup_closed ax);auto;
  try auto_meet_intro;
  auto_meet_elim_trans.

Ltac auto_up_closed X:= now apply tup_closed with X;[auto_meet_intro_elim|].
Ltac auto_add_inf:=
  unfold entails,nentails;
  match goal with
  |[ |- tsep ?p ]=>
  apply tup_closed with (meet_set (fun x => ∃(b:X), x=(p))) ;auto_meet_leq;[repeat auto_empty_meet_elim|]
end.




Lemma recognize_img_neg `{I:Type} (a :I→X):
  ( ⋂[i] (¬(a i)) = ⊓ (image_set tneg (λ x, ∃ i, x = (a i)) )).
Proof.
  apply meet_same_set;split;unfold image_set,Included, In;intros.
  - destruct H as (i,H).
      later. split;[exists i;reflexivity|]. intuition. 
  - destruct H as (c,((i,Hi),H));subst.
      exists i;reflexivity.
Qed.


Lemma recognize_img_tensor_r `{I:Type} (a:X) (b :I→X):
  ( ⋃[i] (a ⊗ (b i)) = ⊔ (@tensor_set_r _ tensor a (λ x,∃i, x=b i))).
Proof.
  apply join_same_set;split;unfold tensor_set_r,Included, In;intros.
  - destruct H as (i,H).
      later. split;[exists i;reflexivity|]. intuition. 
  - destruct H as (c,((i,Hi),H));subst.
      exists i;reflexivity.
Qed.

Lemma recognize_img_tensor_l `{I:Type} (b:X) (a :I→X):
  ( ⋃[i] (a i ⊗ b ) = ⊔ (@tensor_set_l _ tensor (λ x,∃i, x=a i) b)).
Proof.
  apply join_same_set;split;unfold tensor_set_l,Included, In;intros.
  - destruct H as (i,H).
      later. split;[exists i;reflexivity|]. intuition. 
  - destruct H as (c,((i,Hi),H));subst.
      exists i;reflexivity.
Qed.



(** ** Generalized axioms *)

(** We can generalize the pair formation and deduction rule
to family of elements. *)

Lemma pair_inf `{I:Type} (a b:I→X):
  ⋂[i] (a i) ∈ʆ → ⋂[i] (b i) ∈ʆ →   ⋂[i] (a i ⊗ b i) ∈ʆ .
Proof.
  intros Ha Hb.
  apply tup_closed with ((∩ a ) ⊗ (∩ b )).
  - auto_meet_intro.
    apply tensor_mon;auto_meet_elim_trans.
  - now apply tpair.
Qed.


Lemma MP_inf `{I:Type} (a b:I→X):
  ⋂[i] (¬((a i)⊗(b i))) ∈ʆ → ⋂[i] (a i) ∈ʆ → ⋂[i] (¬((b i))) ∈ʆ.
Proof.
  intros Hab Ha.
  rewrite recognize_img_neg in Hab.
  rewrite recognize_img_neg.
  rewrite <- tneg_join in Hab.
  rewrite <- tneg_join .
  apply tmodus_ponens with (⋂[i] a i).
  intuition.
  eapply tup_closed;[|exact Hab].
  apply tneg_mon.
  rewrite tensor_join_r.
  unfold tensor_set_r.
  auto_join_intro.
  intros x (c,((i,Hi),Hc));subst.
  auto_join_elim_trans.
  apply tensor_mon_l.
  auto_meet_elim_risky.
Qed.


(** Commutation of the tensor wrt. to the separator. *)

Lemma tens_comm_sep_inf `{I:Type} (a b:I→X): ⋂[i] (¬(a i ⊗ b i)) ∈ʆ → ⋂[i](¬(b i⊗ a i)) ∈ʆ.
Proof.
  intros Hab.
  apply MP_inf with (λ i, (¬(a i⊗b i)));auto.
  apply (tup_closed TS3);auto.
  auto_meet_intro.
  auto_meet_elim_trans.
Qed.

Lemma const_inf `{I:Type} (a:I→X) a0 (i:I): (∀ i, a i =a0) → ⋂[i] a i = a0.
Proof.
  intros H.
  apply ord_antisym.
  - auto_meet_elim_risky. now rewrite (H i).
  - auto_meet_intro. now rewrite H.
Qed.
  
Lemma tens_comm_sep (a b:X): (¬(a ⊗ b)) ∈ʆ →(¬(b ⊗ a)) ∈ʆ.
Proof.
  intros Hab.
  rewrite <- (const_inf (λ i:nat, ¬(b⊗ a)) 0).
  apply tens_comm_sep_inf.
  rewrite (@const_inf nat (λ i:nat, ¬(a⊗ b)) (¬(a⊗b)) 0);intuition.
  auto. 
Qed.



(** Associativity of the tensor wrt. to the separator. *)

  
Lemma tens_assoc_l_sep_inf `{I:Type} (a b c:I→X):
  (⋂[i] (¬(a i ⊗ b i ⊗ c i))) ∈ʆ -> (⋂[i](¬((a i⊗b i)⊗c i))) ∈ʆ.
Proof.
  intro H.
  apply MP_inf with (λ i, ¬(a i⊗b i⊗c i));auto.
  apply (tup_closed TS5);auto.
  auto_meet_intro. auto_meet_elim_trans.
Qed.

Lemma tens_assoc_r_sep_inf `{I:Type} (a b c:I→X):
  (⋂[i](¬((a i⊗b i)⊗c i))) ∈ʆ → (⋂[i] (¬(a i ⊗ b i ⊗ c i))) ∈ʆ.
Proof.
  intro H.
  now apply tens_comm_sep_inf,tens_assoc_l_sep_inf,tens_comm_sep_inf,tens_assoc_l_sep_inf,tens_comm_sep_inf.
Qed.

Lemma tens_assoc_sep_inf `{I:Type} (a b c:I→X):
  ⋂[i] (¬(a i⊗ b i⊗ c i)) ∈ʆ <-> ⋂[i] (¬((a i ⊗ b i)⊗ c i)) ∈ʆ.
Proof.
  split;intro H;[now apply tens_assoc_l_sep_inf|now apply tens_assoc_r_sep_inf].
Qed.


Lemma tens_assoc_sep: ∀ (a b c:X), (¬(a ⊗ b ⊗ c)) ∈ʆ <-> (¬((a⊗b)⊗c)) ∈ʆ.
Proof.
  intros a b c.
  rewrite <- (@const_inf nat (λ i:nat, ¬(((a⊗ b)⊗c))) (¬((a⊗ b)⊗c)) 0);auto.
  rewrite <- (const_inf (λ i:nat, ¬((a⊗ b⊗c))) 0);auto.
  apply tens_assoc_sep_inf.
Qed.

Lemma tens_assoc_l_sep: ∀ (a b c:X), (¬(a ⊗ b ⊗ c)) ∈ʆ -> (¬((a⊗b)⊗c)) ∈ʆ.
Proof.
  intros a b c;intro H. now apply tens_assoc_sep.
Qed.

Lemma tens_assoc_r_sep: ∀ (a b c:X), (¬((a⊗b)⊗c)) ∈ʆ → (¬(a ⊗ b ⊗ c)) ∈ʆ.
Proof.
  intros a b c;intro H. now apply tens_assoc_sep.
Qed.


(** We use S4 to show arrow to compose an hypothesis in the separator 
with an arrow c⊢⊗b *)


Lemma arrow_compat_r_inf `{I:Type} (a b c:I→X):
  ⋂[i](¬(a i⊗b i)) ∈ʆ → c [i]⊢⊗ b → ⋂[i](¬(a i⊗c i)) ∈ʆ.
Proof.
  intros Hab Hcb.
  eapply MP_inf;[|exact Hab].
  eapply MP_inf with (λ i,¬(¬b i⊗c i)).
  - auto_up_closed TS4. 
  - now apply tens_comm_sep_inf.
Qed.

Lemma arrow_compat_r a b c: (¬(a⊗b)) ∈ʆ → c ⊢⊗ b → (¬(a⊗c)) ∈ʆ.
Proof.
  intros Hab Hcb.
  eapply tmodus_ponens;[exact Hab|].
  apply tmodus_ponens with (¬(¬b⊗c)).
  - now apply tens_comm_sep.
  - apply tup_closed with TS4;intuition.
    auto_meet_elim_risky.
Qed.

Lemma arrow_compat_l_inf `{I:Type} (a b c:I→X):
  ⋂[i](¬(b i⊗a i)) ∈ʆ → c [i]⊢⊗ b → ⋂[i](¬(c i⊗a i)) ∈ʆ.
Proof.
  intros Hab Hcb.
  apply tens_comm_sep_inf.
  eapply arrow_compat_r_inf;[|exact Hcb].
- now apply tens_comm_sep_inf.
Qed.


Lemma arrow_compat_l a b c: (¬(b⊗a)) ∈ʆ → c ⊢⊗ b → (¬(c⊗a)) ∈ʆ.
Proof.
  intros Hab Hcb.
  apply tens_comm_sep.
  eapply arrow_compat_r;[|exact Hcb].
  now apply tens_comm_sep.
Qed.


Ltac under_arrow_l ax:= eapply arrow_compat_l;[|apply ax].
Ltac under_arrow_r ax:= eapply arrow_compat_r;[|apply ax].



(** Transitivity of the ⊢¬ relation. *)

Lemma C6_n_inf `{I:Type} (a b c:I→X): ⋂[i](¬(a i ⊗ b i)) ∈ʆ → ⋂[i](¬((¬(b i) ⊗ c i))) ∈ʆ → ⋂[i](¬(a i ⊗ c i)) ∈ʆ.
Proof.
  intros Sab Sbc.
  apply (MP_inf (λ i, (¬(a i ⊗ b i))));[|apply Sab].
  apply (MP_inf (λ i, (¬(¬(b i) ⊗ c i))));[|apply Sbc].
  apply tup_closed with TS4;auto.
  auto_meet_intro.
  auto_meet_elim_trans.
Qed.

Lemma C6n_l_inf `{I:Type} (a b c:I→X): ⋂[i](¬(a i ⊗ b i)) ∈ʆ → ⋂[i]((c i ⊗↦ a i)) ∈ʆ → ⋂[i](¬(c i ⊗ b i)) ∈ʆ.
Proof.
  intros Sab Sbc. 
  apply tens_comm_sep_inf in Sab.
  apply tens_comm_sep_inf in Sbc.
  apply tens_comm_sep_inf.
  apply C6_n_inf with a;assumption.
Qed.

Lemma C6n_r_inf `{I:Type} (a b c:I→X): ⋂[i](¬(a i ⊗ b i)) ∈ʆ → ⋂[i]((c i ⊗↦ b i)) ∈ʆ → ⋂[i](¬(a i ⊗ c i)) ∈ʆ.
Proof.
  intros Sab Sbc.
  apply tens_comm_sep_inf in Sbc.
  apply C6_n_inf with b;assumption.
Qed.


Lemma C6_n a b c: (a ⊢ ¬ b) → ((¬b) ⊢ ¬ c) → (a ⊢ ¬ c).
Proof.
  intros Sab Sbc.
  apply (tmodus_ponens (¬(a ⊗ b)));[apply Sab|].
  apply (tmodus_ponens (¬(¬b ⊗ c)));[apply Sbc|].
  apply tup_closed with TS4;auto.
  auto_meet_elim_trans.
Qed.


(** *** Axioms rephrased with ⊢¬ .*) 


Lemma S1_n : ∀ (a:X),  (¬(a ⊗ a)) ⊢ ¬ a.
Proof.
  auto_sep_Si TS1.
Qed.


Lemma S2_n : ∀ a b, (¬a) ⊢ ¬ (a⊗b).
Proof.
  auto_sep_Si TS2.
Qed.

Lemma S3_n : ∀ a b, (¬(a ⊗ b))  ⊢ ¬ (b ⊗ a).
Proof.
  auto_sep_Si TS3.
Qed.


Lemma S4a_n_inf `{I:Type} (a b c:I→X): 
(λ i, a i⊗↦ b i) [i]⊢¬ (λ i,¬(c i⊗ b i) ⊗ (c i⊗ a i)).
Proof.
  intros.
  apply C6_n_inf with (λ i,(¬ (b i)⊗ a i)).
  -  apply tup_closed with TS3;auto.
  auto_meet_intro.  apply meet_elim_trans.
  eexists;split;[exists (a a0);reflexivity|].
  apply meet_elim.  exists (¬(b a0)). reflexivity.
  -  auto_sep_Si TS4.
Qed.


Lemma S4a_n : ∀ a b c,(a ⊗↦ b) ⊢ ¬ (¬(c ⊗ b) ⊗ (c ⊗ a)).
Proof.
  intros.
  apply C6_n with ((¬b ⊗ a)).
  apply S3_n.
  auto_sep_Si TS4.
Qed.

Lemma S5_n : ∀ a b c, (¬(a ⊗ (b⊗c)))  ⊢ ¬ ((a ⊗ b)⊗c).
Proof.
  auto_sep_Si TS5.
Qed.


Lemma S4b_t_inf `{I:Type} (a b c:I→X): 
(⋂[i] ¬((a i⊗↦ b i) ⊗ ((c i⊗ a i) ⊗ ¬(c i⊗ b i)))) ∈ʆ.
Proof.
  intros.
  apply C6_n_inf with (λ i,(¬(c i⊗ b i) ⊗ (c i⊗ a i))).
  apply S4a_n_inf.
  auto_sep_Si TS3.
Qed.


Lemma S4b_t : ∀ a b c,(a ⊗↦ b) ⊢ ¬ ((c ⊗ a) ⊗ ¬(c ⊗ b)).
Proof.
  intros.
  apply C6_n with ((¬(c ⊗ b) ⊗ (c ⊗ a))).
  apply S4a_n.
  apply S3_n.
Qed.

Lemma id_n_inf `{I:Type} a: (λ i:I, ¬ (a i)) [i]⊢¬ a.
Proof.
  apply C6_n_inf with (λ i, a i⊗a i).
  +  auto_sep_Si TS2.
  +  auto_sep_Si TS1.
Qed.      

Lemma id_n a: (¬a) ⊢ ¬ a.
Proof.
  apply C6_n with (a⊗a).
  + apply S2_n.
  + apply S1_n.
Qed.      


(** *** Axioms rephrased with ⊢⊗ .*) 


Lemma id_t_inf `{I:Type} (a:I→X): a [i]⊢⊗ a.
Proof.
  apply tens_comm_sep_inf.
  apply id_n_inf.
Qed.

Lemma id_t a: a ⊢⊗ a.
Proof.
  apply tens_comm_sep.
  apply id_n.
Qed.

Lemma S1_t_inf `{I:Type} (a:I→X):
  (⋂[i]((a i) ⊗↦ (a i ⊗ a i))) ∈ʆ.
Proof.
  apply tens_comm_sep_inf.
  apply tup_closed with TS1;auto.
  auto_meet_intro_elim.
Qed.

Lemma S1_t : ∀ (a:X),  a ⊢⊗ (a⊗a).
Proof.
  intros. auto_add_inf. apply S1_t_inf.
Qed.


Lemma S2_t : ∀ a b, (a⊗b) ⊢⊗ a.
Proof.
  intros;apply tens_comm_sep;apply S2_n.
Qed.


Lemma S3_t_inf `{I:Type} (a b:I→X):
 ⋂[i] ((a i⊗ b i)  ⊗↦ (b i⊗ a i))∈ʆ.
Proof.
  intros;apply tens_comm_sep_inf.
  auto_up_closed TS3.
Qed.


Lemma S3_t : ∀ a b, (a ⊗ b)  ⊢⊗ (b ⊗ a).
Proof.
  intros. auto_add_inf. apply S3_t_inf.
Qed.


Lemma S4_t_inf  `{I:Type}  (a b c :I →X): 
      (a [i]⊢⊗ b) → (λ i, (c i ⊗ a i))  [i]⊢⊗ (λ i, c i⊗ b i).
Proof.
  intros. apply tens_comm_sep_inf.
  eapply MP_inf. apply S4a_n_inf.
  assumption.
Qed.


Lemma S4_t : ∀ a b c,(a ⊢⊗ b) →  (c ⊗ a) ⊢⊗ (c ⊗ b).
Proof.
  intros. apply tens_comm_sep.
  eapply tmodus_ponens;[|apply S4a_n].
  assumption.
Qed.


Lemma S5_t_inf `{I:Type}  (a b c :I →X):
  ⋂[i](((a i⊗ b i) ⊗ c i) ⊗↦ (a i⊗(b i ⊗ c i)))∈ʆ.
Proof.
  intros. apply tens_comm_sep_inf. auto_up_closed TS5. 
Qed.


Lemma S5_t : ∀ a b c, ((a ⊗ b ) ⊗ c ) ⊢⊗ (a ⊗(b ⊗ c)).
Proof.
  intros. auto_add_inf. apply S5_t_inf.
Qed.


(** *** Variants of the axiom S4.*) 


Lemma S4c_n_inf  `{I:Type}  (a b c :I →X): 
      (a [i]⊢⊗ b) → (λ i, ¬(c i ⊗ b i))  [i]⊢¬ (λ i, c i⊗ a i).
Proof.
  intros.
  apply tens_comm_sep_inf.
  apply S4_t_inf.
  assumption.
Qed.

Lemma S4c_n : ∀ a b c,(a ⊢⊗ b) → ( ¬(c ⊗ b)) ⊢ ¬ (c ⊗ a).
Proof.
  intros. unfold nentails.
  apply tens_comm_sep, S4_t.
  assumption.
Qed.


Lemma S4d_n : ∀ a b c,(a ⊢⊗ b) → ( ¬(c ⊗ b)) ∈ʆ →  (¬ (c ⊗ a))∈ʆ.
Proof.
  intros.
  apply tmodus_ponens with (¬(c⊗b));intuition.
  apply S4c_n.
  assumption.
Qed.


Lemma S4d_n_inf `{I:Type}  (a b c :I →X): 
  (a [i]⊢⊗ b) → ⋂[i]( ¬(c i⊗ b i)) ∈ʆ →  ⋂[i](¬ (c i⊗ a i))∈ʆ. 
Proof. 
   intros. 
   apply MP_inf with (λ i,¬(c i⊗b i));intuition. 
   apply S4c_n_inf. 
   assumption. 
 Qed. 


(** Contraposition. *)
Lemma contrapos_inf `{I:Type} (a b:I→X) : (a [i]⊢⊗ b) → ((¬i b) [i]⊢⊗ ¬i a).
Proof.
  intros Hab. 
  apply C6_n_inf with a;apply tens_comm_sep_inf.
  assumption.
  apply id_n_inf.
Qed.


Lemma contrapos a b : (a ⊢⊗ b) → ((¬b) ⊢⊗ ¬a).
Proof.
  intros Hab.
  apply C6_n with a;apply tens_comm_sep.
  assumption.
  apply id_n.
Qed.



(** Transitivity. *)

Lemma C6_t a b c: (a ⊢⊗ b) → (b ⊢⊗ c) → (a ⊢⊗ c).
Proof.
 intros Sab Sbc.
  eapply C6_n; unfold nentails,tarrow.
  - apply Sab.
  - apply tens_comm_sep.
    now apply contrapos.
Qed.




Lemma C6_t_inf  `{I:Type} (a b c:I→X):
(a [i]⊢⊗ b) → (b [i]⊢⊗ c) → (a [i]⊢⊗ c).
Proof.
 intros Sab Sbc.
  eapply C6_n_inf.
  - apply Sab.
  - apply tens_comm_sep_inf.
    now apply contrapos_inf.
Qed.


(** Extra lemmas *)


Lemma S5b_t_inf `{I:Type} (a b c:I→X):
  ⋂[i] ((a i ⊗(b i⊗ c i)) ⊗↦((a i ⊗ b i) ⊗ c i))∈ʆ.
Proof.
  intros.
  eapply C6_t_inf;[apply S3_t_inf|].
  eapply C6_t_inf;[apply S5_t_inf|].
  eapply C6_t_inf;[apply S3_t_inf|].
  eapply C6_t_inf;[apply S5_t_inf|].
  apply S3_t_inf.
Qed.


  
Lemma S5b_t : ∀ a b c,(a⊗(b ⊗ c)) ⊢⊗((a ⊗b) ⊗ c).
Proof.
  intros. auto_add_inf. apply S5b_t_inf.
Qed.


Lemma S4_t_l_inf `{I:Type} (a b c:I→X):
  a [i]⊢⊗ b → ⋂[i] ((a i ⊗ c i) ⊗↦(b i ⊗ c i))∈ʆ. 
Proof.
  intros H.
  eapply C6_t_inf;[apply S3_t_inf|].
  eapply C6_t_inf;[|apply S3_t_inf].
  apply S4_t_inf,H.
Qed.


Lemma S4_t_l a b c: a ⊢⊗ b → (a ⊗ c) ⊢⊗ (b ⊗ c).
Proof.
  intros H.
  eapply C6_t;[apply S3_t|].
  eapply C6_t;[|apply S3_t].
  apply S4_t,H.
Qed.



Lemma extra_hyp_r a b: (¬a) ∈ʆ → (¬(a⊗b))∈ʆ.
Proof.
intros Ha.
apply tmodus_ponens with (¬a);intuition.
auto_sep_Si TS2.
Qed.

Lemma extra_hyp_l a b: (¬a) ∈ʆ → (¬(b⊗a))∈ʆ.
Proof.
intros Ha. apply tens_comm_sep. now apply extra_hyp_r.
Qed.



(** *** Properties of the negation. *)


Lemma dne_t_inf `{I:Type} (a:I→X): (λ i, ¬¬ (a i)) [i]⊢⊗ a.
Proof.
  apply tens_comm_sep_inf.
  apply id_t_inf.
Qed.

Lemma dni_n_inf `{I:Type} (a:I→X): ⋂[i]( ¬(a i ⊗ ¬(a i))) ∈ʆ.
Proof.
  apply id_t_inf.
Qed.

Lemma dni_t_inf `{I:Type} (a:I→X): (⋂[i] (a i ⊗↦ (¬¬ (a i)))) ∈ʆ.
Proof.
  apply C6_n_inf with (¬i a);
  apply dni_n_inf.
Qed.


Lemma dne_n_inf `{I:Type} (a:I→X): (⋂[i] ¬(¬¬¬ (a i) ⊗ a i))∈ʆ.
Proof.
  apply tens_comm_sep_inf.
  apply dni_t_inf.
Qed.



Lemma dne_t a: (¬¬a) ⊢⊗ a.
Proof.
  apply tens_comm_sep.
  apply id_t.
Qed.

Lemma dni_n a: a ⊢ ¬ (¬a).
Proof.
  unfold nentails.
  apply id_t.
Qed.

Lemma dni_t a: (a) ⊢⊗ ¬¬a.
Proof.
  apply C6_n with (¬a).
  apply dni_n.
  apply dni_n.
Qed.


Lemma dne_n a: (¬¬¬a) ⊢ ¬ (a).
Proof.
  unfold nentails.
  apply tens_comm_sep.
  apply dni_t.
Qed.

Hint Resolve dne_t dni_t dne_n dni_n.


Lemma ex_falso a: ⊥ ⊢⊗ a.
Proof.
  eapply (tup_closed );[|apply id_t].
  apply tarrow_mon_r. intuition.
Qed.




(** Contraposition, bis. *)

Lemma contraposition_inf `{I:Type} (a b:I→X): 
  ⋂[i] (a i ⊗↦ b i)∈ʆ <-> ⋂[i]((¬(b i))⊗↦¬ (a i))∈ʆ. 
Proof. 
 split;intros. 
  - apply S4d_n_inf with a. 
    + apply dne_t_inf. 
    + apply tens_comm_sep_inf. intuition. 
  -  apply tens_comm_sep_inf. apply S4d_n_inf with (¬i ¬i a). 
    + apply dni_t_inf.
    + intuition. 
 Qed.     
  

Lemma contraposition a b: (a ⊗↦ b)∈ʆ <-> ((¬b)⊗↦¬a)∈ʆ.
Proof.
 split;intros.
 - unfold tarrow.
   apply S4d_n with a.
   + apply dne_t.
   + apply tens_comm_sep. intuition.
 -  apply tens_comm_sep. apply S4d_n with (¬¬a).
   + apply dni_t.
   + intuition.
Qed.    

Lemma contrapos_t_inf `{I:Type} (a b:I→X):
  ⋂[i] ((a i⊗↦ b i) ⊗↦ ((¬b i) ⊗↦ ¬ (a i)))∈ʆ.
Proof.
  eapply arrow_compat_r_inf with (b:=λ i, ¬(a i⊗↦ b i)).
  apply id_t_inf.
  do 2 (apply contrapos_inf). 
  eapply arrow_compat_l_inf with (λ i,¬ b i ⊗ a i);[|  apply S4_t_inf, dne_t_inf]. 
  eapply arrow_compat_l_inf with (λ i, a i ⊗ ¬ b i);[apply id_t_inf|apply S3_t_inf]. 
Qed. 


Lemma contrapos_t a b : (a ⊗↦ b) ⊢⊗ ((¬b) ⊗↦ ¬a).
Proof.
  auto_add_inf. apply contrapos_t_inf.
Qed.



(** Variants of S4, bis. *)

Lemma S4c_t_inf `{I:Type} (a b c:I→X):
  ⋂[i]((a i⊗↦ b i) ⊗↦ (c i⊗ a i) ⊗↦ (c i⊗ b i)) ∈ʆ.
Proof.
  intros.
  unfold tarrow.   
  eapply C6_n_inf ;[apply S4b_t_inf|].
  unfold nentails,tarrow.
  eapply arrow_compat_r_inf;[|apply dne_t_inf].
  apply id_n_inf.
Qed.


Lemma S4c_t : ∀ a b c,(a ⊗↦ b) ⊢⊗ ((c ⊗ a) ⊗↦(c ⊗ b)).
Proof.
  intros;auto_add_inf;apply S4c_t_inf.
Qed.


Lemma S4d_t_inf `{I:Type} (a b c:I→X):
  ⋂[i]((a i⊗↦ b i) ⊗↦ (c i⊗↦ a i) ⊗↦ c i⊗↦ b i) ∈ʆ.
Proof.
  intros.
  eapply C6_t_inf; [apply contrapos_t_inf|]. 
  eapply C6_t_inf;[|apply contrapos_t_inf].
  apply S4c_t_inf.
Qed.

Lemma S4d_t : ∀ a b c,(a ⊗↦ b) ⊢⊗ ((c ⊗↦ a) ⊗↦(c ⊗↦ b)).
Proof.
  intros.
  eapply C6_t;[apply contrapos_t|].
  eapply C6_t;[|apply contrapos_t].
  apply S4c_t.
Qed.





(** Back to properties of the negation. *)

(** We have the equivalence between ¬a and a ⊗↦ ⊥ *)

Lemma tneg_imp_bot: ∀ a, (¬ a) ⊢⊗ a ⊗↦ ⊥.
Proof.
  intro a.
  unfold entails,tarrow.
  apply S4d_n with (a⊗¬⊥).
  apply dne_t.
  apply S2_n.
Qed. 

Lemma tneg_bot_top: (¬ ⊥)=⊤ .
Proof.
  rewrite <- join_emptyset.
  rewrite <- meet_emptyset.
  rewrite tneg_join.
  apply meet_same_set.
  split;intros;unfold Included,In,image_set,emptyset;(intros x H);contradict H.
  intros (a,(Ha,_)). contradict Ha.
Qed.

Lemma top_sep: tsep ⊤ .
Proof.
  apply tup_closed with TS1;intuition.
Qed.

Lemma imp_bot_tneg: ∀ a, a ⊗↦ ⊥  ⊢⊗ (¬ a).
Proof.
  intro a.
  unfold entails,tarrow.
  apply (contraposition a).
  rewrite tneg_bot_top.
  apply (tup_closed (a⊗↦(a⊗a))).
  - apply tarrow_mon_r.
    now apply tensor_mon_r.
  - exact (S1_t a).
Qed.







(** Technical lemmas **)





Lemma dne_under_tensor a b: (a ⊗ ¬¬b)⊗↦(a⊗b) ∈ʆ.
Proof.
  eapply tmodus_ponens.
  2:apply S4b_t.
    apply dne_t.
Qed.

Lemma dni_under_tensor a b: (a ⊗ b)⊗↦(a⊗¬¬b) ∈ʆ.
Proof.
  eapply tmodus_ponens.
  2: apply S4b_t.
    apply dni_t.
Qed.

Lemma dne_under_arrow a b: (¬(a ⊗ ¬¬b))⊗↦ (¬(a⊗ b)) ∈ʆ.
Proof.
  apply (contraposition (a⊗b)).
  apply dni_under_tensor.
Qed.

Lemma dni_under_arrow a b: (¬(a ⊗ b))⊗↦(¬(a⊗ ¬¬b)) ∈ʆ.
Proof.
  apply (contraposition _ (a⊗b)).
  apply dne_under_tensor.
Qed.

Lemma dne_under_tensor_inf `{I:Type} (a b:I→X):
 ⋂[i] ((a i⊗ ¬¬ (b i))⊗↦(a i⊗b i)) ∈ʆ.
Proof.
  eapply MP_inf.
  apply S4b_t_inf.
  apply dne_t_inf.
Qed.

Lemma dni_under_tensor_inf `{I:Type} (a b:I→X):
 ( ⋂[i] ( (a i⊗ b i)⊗↦(a i⊗¬¬ b i))) ∈ʆ.
Proof.
  eapply MP_inf.
  apply S4b_t_inf.
  apply dni_t_inf.
Qed.

Lemma dne_under_arrow_inf `{I:Type} (a b:I→X):
 (⋂[i]( (¬(a i ⊗ ¬¬b i))⊗↦ (¬(a i⊗ b i)))) ∈ʆ.
Proof.
  apply (contraposition_inf (λ i,a i⊗b i)).
  apply dni_under_tensor_inf.
Qed.

Lemma dni_under_arrow_inf `{I:Type} (a b:I→X):
 ( ⋂[i] ( (¬(a i⊗ b i))⊗↦¬(a i⊗¬¬ b i))) ∈ʆ.
Proof.
  apply (contraposition_inf _ (λ i,a i⊗b i)).
  apply dne_under_tensor_inf.
Qed.







(** *** Computational adjunction *)
Definition antiapp t u:= (⊔(λ x:X, ∃ a:X, x= a ∧ (t ≤ ¬ (u ⊗ a) ))).
Notation "a ∅ b ":=(antiapp a b) (at level 56).

Definition tabs t:=( ⊓(λ x:X, ∃ a:X, x=a⊗↦t a)).
Definition tapp a b:=⊓ (λ x, ∃ c, x = (¬¬c) /\ a ≤ b ⊗↦ c).
Notation "t @⊗ u":=(tapp t u) (at level 59, left associativity).
Notation "λ⊗ t":=(tabs t) (at level 60).

Transparent tabs tapp tarrow.


Lemma tabs_ext : ∀ (f g:X→X),(∀x,f x = g x) → (tabs f) = tabs g.
Proof.
  intros f g eqfg.
  apply meet_same_set;split;unfold Included,In; intros x (a,Hx);exists a;rewrite Hx;now f_equal.
Qed.
Hint Resolve tabs_ext.
  
Lemma tadj a b c: c ≤ antiapp a b  <-> a ≤ ¬(b⊗c).
Proof.
  unfold antiapp; split;intro H.
  - apply ord_trans with (¬(b ⊗ (antiapp a b))).
    + unfold antiapp. rewrite tensor_join_r.
      rewrite tneg_join.
      apply meet_intro.
      unfold image_set,tensor_set_r.
      intros x (d,((e,((f,(Hf,Hg)),He)),Hd));subst.
      intuition.
    + apply tneg_mon.
      now apply tensor_mon_r.
  - apply join_elim.
    exists c;split;intuition.
Qed.
    

Lemma tadj_imp a b c: (¬c) ≤ antiapp a b  <-> a ≤ ¬(b⊗¬c).
Proof.
  exact (tadj a b (¬c)).
Qed.


Lemma tadj_antiapp a b: a ≤ ¬(b⊗(a∅b)).
Proof.
  now apply tadj.
Qed.


(** Separators are closed under antiapplication. *)
Lemma antiapp_closed a b: a∈ʆ → b ∈ʆ → ¬(a∅b) ∈ʆ.
Proof.
  intros Ha Hb.
  eapply tmodus_ponens with b.
  2:{ eapply tup_closed.
      apply tadj.
      reflexivity.
      assumption. }
  assumption.
Qed.


(** About the application. *)
      
Lemma app_join a b: a@⊗b = ¬ (⊔ (λ x, ∃ c, x = (¬c) /\ a ≤ b ⊗↦ c)).
Proof.
  unfold antiapp.
  rewrite tneg_join.
  apply meet_same_set;split;unfold image_set,Included,In;intros c H;
    repeat (destruct H;subst).
  -  later;split. later;split.
     2:{ exact H0. } reflexivity. reflexivity.
  -  later;split.  reflexivity. assumption.
Qed.

Lemma app_closed a b: a∈ʆ → b ∈ʆ → (a @⊗ b) ∈ʆ.
Proof.
  intros Ha Hb.
  unfold app.
  eapply tup_closed.
  apply meet_intro;intros x (c,(Hc,Hx));subst.
  unfold tarrow in Hx.
  rewrite <- tadj_imp in Hx.
  apply tneg_mon;exact Hx.
  now apply antiapp_closed.
Qed.

  
Lemma demiadj_app a b c: a ≤ (¬(b⊗¬c)) -> (a@⊗ b) ≤ ¬¬c.
Proof.
  intro H.
  rewrite <- tadj_imp in H.
  unfold antiapp in *.
  rewrite app_join.
  apply tneg_mon.
  apply join_elim.
  later;split;try reflexivity.
  eapply ord_trans.
  2:{apply tneg_mon.
  apply tensor_mon_r;exact H.
  }
  now apply tadj.
Qed.

Ltac auto_dni_tensor_r:=
    eapply C6_n_inf;[|apply dni_n_inf]; simpl.
Ltac auto_dne_tensor_r:=
    eapply C6_n_inf;[|apply dne_n_inf]; simpl.



Lemma pack_inf `{I:Type} (a:I → X) (P:X → X → Prop):
  let J:={j:(I*X) & let (i,d):=j in  P (a i) d} in
  let a':=(λ t:J, let (x,_):=t in let (i,_):=x in  a i) in
  let d':=(λ t:J, let (x,_):=t in let (_,z):=x in z) in
  ⋂[ i] ⊓ (λ x0 : X, ∃ d : X, x0 = (¬ ( a i  ⊗ d))                                                 ∧ P (a i) d)
  =  ( ⋂[ i] (¬ (a' i ⊗ (d' i)))) .
Proof.
  intros.
  apply ord_antisym;repeat auto_meet_intro;rename a0 into i.
  - destruct i as ((i,d),p).
    auto_meet_elim_trans.
    auto_meet_elim.
    exists d. split;[|exact p].
    reflexivity.
  - auto_meet_elim. exists (existT  _ (i,a1) H1).
    simpl. reflexivity.
Qed.    



Lemma app_entails_aux :⋂[a] (⋂[b](a ⊗↦ (b ⊗↦ a@⊗ b )))∈ʆ.
Proof.
  pose (I:=prod X  X).
  pose (a:=(λ i : I, let (a,_):=i in a)).
  pose (b:=(λ i : I, let (_,b):=i in b)).
  apply tup_closed with (⋂[ i] (a i ⊗↦ b i ⊗↦ ¬ (⊔ (λ x, ∃ c, x = (¬c) /\ a i≤ b i⊗↦ c)))).
  1:{ auto_meet_intro_elim. instantiate (3:=(a0,a1)).
      repeat (apply tarrow_mon_r). rewrite app_join.
      reflexivity. }
  auto_dni_tensor_r.
  rewrite tens_assoc_sep_inf.
  auto_dni_tensor_r.
  pose (J:={j:(I*X) & let (i,c):=j in a i  ≤ b i  ⊗↦ c}).
  pose (a':=(λ t:J, let (j,_):=t in let (i,_):=j in a i)).
  pose (b':=(λ t:J, let (j,_):=t in let (i,_):=j in b i)).
  pose (c':=(λ t:J, let (j,_):=t in let (_,c):=j in c)).
  apply tup_closed with
  (⋂[ j] (¬ ((a' j ⊗ b' j) ⊗ ¬ (c' j)))).
  1: { repeat auto_meet_intro.
       rewrite tensor_join_r,tneg_join.
       auto_meet_intro.
       intros y (?b,((?d,((?c,(Hc,Hd)),Ha)),Hb)).
       subst. apply meet_elim.
       exists (existT (λ j:(I * X),let (i, c) := j in a i ≤ b i ⊗↦ c) (a0,c) Hd).
       reflexivity.
       }
  apply tens_assoc_l_sep_inf.
  auto_dne_tensor_r.
  apply tup_closed with (⋂[a] (¬(a⊗¬a)));[| apply id_t_inf].
  auto_meet_intro.
  destruct a0 as (((y,z),c),p). simpl in *.
  auto_meet_elim_trans.
  apply tarrow_mon_r. assumption.
Qed.

Lemma app_entails_inf `{I:Type} (a b:I→X) :
  ⋂[i] (a i ⊗↦ (b i ⊗↦ a i@⊗ b i))∈ʆ.
Proof.
  eapply tup_closed;[|apply app_entails_aux].
  auto_meet_intro_elim.
Qed.  

Lemma app_entails_bis a b:(a ⊗↦ (b ⊗↦ a@⊗ b ))∈ʆ.
Proof.
  eapply tup_closed;[|apply app_entails_aux].
  auto_meet_intro_elim.
Qed.


Lemma app_entails a b c: a @⊗ b  ≤ c  → (a ⊗↦ (b ⊗↦ c ))∈ʆ.
Proof.
  intro H.
  apply C6_t with (b ⊗↦ a@⊗b);[apply app_entails_bis|].
  apply tup_closed with (⋂[a] (¬(a⊗¬a)));[| apply id_t_inf].
  auto_meet_elim_trans.
  do 2 (apply tarrow_mon_r). assumption.  
Qed.

Lemma arrow_switch_premises_inf `{I:Type} (a b c:I→X):
  ⋂[i] ((a i⊗↦ b i⊗↦ c i) ⊗↦ b i⊗↦ a i⊗↦ c i)∈ʆ.
Proof.
  apply C6_t_inf  with (λ i,¬(a i⊗ b  i⊗ ¬ c i)).
  - apply contraposition_inf with (a:=λ i,a i⊗ b i ⊗ ¬ c i).
    apply dni_under_tensor_inf.
  - apply C6_t_inf with (λ i,¬(b i⊗ a i ⊗ ¬ c i)).
    + apply contraposition_inf with (a:=λ i,b i⊗ a i⊗ ¬ c i).
      eapply C6_t_inf;[apply S5b_t_inf|].
      eapply C6_t_inf;[apply S3_t_inf|].
      eapply C6_t_inf;[|apply S5_t_inf].
      eapply C6_t_inf;[|apply S3_t_inf].
      apply S4_t_inf, S3_t_inf.
    + apply contraposition_inf with (b:=λ i, b i⊗ a i⊗ ¬ c i).
    apply dne_under_tensor_inf.
Qed.

Lemma arrow_switch_premises a b c:
  (a ⊗↦ b ⊗↦ c) ⊗↦ (b ⊗↦ a ⊗↦ c) ∈ʆ.
Proof.
  auto_add_inf. apply arrow_switch_premises_inf.
Qed.


Lemma S4e_t_inf `{I:Type} (a b c:I→X):
  ⋂[i](a i⊗↦ b i) ∈ʆ → ⋂[i]((c i ⊗↦ a i) ⊗↦ (c i⊗↦ b i))∈ʆ.
Proof.
  intros.
  apply contraposition_inf with (b:=λ i,c i⊗¬ (a i)).
  apply tens_comm_sep_inf, S4c_n_inf.
  now apply (contraposition_inf) with (a:=a). 
Qed.
                                                 
Lemma S4e_t : ∀ a b c,(a ⊗↦ b) ∈ʆ → ((c ⊗↦ a) ⊢⊗(c ⊗↦ b)).
Proof.
  intros.
  apply contraposition with (b:=c⊗¬a).
  apply tens_comm_sep, S4c_n.
  now apply contraposition with (a:=a).
Qed.



(** *** Combinators K and S *)



(** We show that the type of K and K viewed as a λ-term 
    both belong to any separator. *)

Lemma tK a b: (a ⊗↦ b ⊗↦ a) ∈ʆ.
Proof.
apply (S4d_n _ _ _ (dne_t (b⊗¬a))).
apply tens_assoc_sep.
apply S2_t.
Qed.


Lemma tsep_K_type:(⋂[a] ⋂[b] (a ⊗↦ b ⊗↦ a)) ∈ʆ.
Proof.
  pose (I:=(prod X X)).
  pose (a:=λ t:I, let (x,y):=t in x).
  pose (b:=λ t:I, let (x,y):=t in y).
  apply tup_closed with (⋂[i] (a i ⊗↦ b i ⊗↦ a i)).
  do 2 auto_meet_intro. apply meet_elim.
  now exists (a0,a1).
  apply S4d_n_inf with (b:=λ i, (b i ⊗¬ (a i))).        
  - apply tens_comm_sep_inf.
    eapply tup_closed;[ | apply id_t_inf with (a:=λ i,b i ⊗↦ a i)].
    repeat (auto_meet_intro). subst. auto_meet_elim.
    now exists a0.
  - apply tens_assoc_r_sep_inf.
    apply tens_comm_sep_inf.
    eapply tup_closed;[|apply tsep_TS2].
    auto_meet_intro_elim.
Qed.




Lemma tsep_K: (λ⊗ (λ x , λ⊗ (λ y,  y))) ∈ʆ.
Proof.
  unfold tabs,tarrow.
  rewrite recognize_img_neg with (λ a, a⊗¬a).
  rewrite <- tneg_join.
  auto_dni_tensor_r.
  pose (I:=(prod X X)).
  pose (a:=(λ t:I, let (x,y):=t in x)).
  pose (b:=(λ t:I, let (x,y):=t in y)).
  eapply tup_closed with (⋂[i] ¬ (a i ⊗ b i ⊗ ¬ b i)).
  - auto_meet_intro.
    rewrite tensor_join_r, tneg_join.
    auto_meet_intro_elim.
    intros c (d,((z,((w,Hw),Hb)),Hz));subst.
    auto_meet_elim. now exists (a0,w).
  - apply  C6_n_inf with (b:= λ i, (¬  b i) ⊗ b i).
    + apply tens_comm_sep_inf.
      apply tens_assoc_sep_inf.
      auto_up_closed TS2.
    + auto_up_closed TS3.
Qed.


(** We show that the type of S and S viewed as a λ-term 
    both belong to any separator. The situation being tricky,
    we first prove a few intermediary lemmas. *)


Lemma tsep_S_inf `{I:Type} (a b c:I→X):
  ⋂[i]((a i⊗↦ b i⊗↦ c i) ⊗↦ (a i⊗↦ b i) ⊗↦ a i⊗↦ c i) ∈ʆ.
Proof.
  eapply C6_t_inf;[apply arrow_switch_premises_inf|].
  eapply C6_t_inf.
  - eapply S4d_t_inf with (c:=λ i, a i).
  - apply S4e_t_inf.
    eapply C6_t_inf. apply contraposition_inf with (a:= λ i,(a i⊗¬¬(a i⊗¬ (c i)))),id_t_inf.
    eapply C6_t_inf. apply contraposition_inf with (a:= λ i,(a i⊗(a i⊗¬(c i)))). 
    apply S4_t_inf,dni_t_inf. 
    apply contraposition_inf with (b:= λ i,(a i⊗(a i⊗¬(c i)))).
    eapply C6_t_inf;[|apply S5_t_inf]. 
    apply S4_t_l_inf, S1_t_inf.
Qed.

Lemma tsep_S_type :
  ⋂[a] (⋂[b] (⋂[c] ((a ⊗↦ b⊗↦ c) ⊗↦ (a ⊗↦ b) ⊗↦ a ⊗↦ c))) ∈ʆ.
Proof.
  pose (I:=prod X (prod X X)).
  pose (a:=(λ t:I, let (x,_):=t in x)).
  pose (b:=(λ t:I, let (_,x):=t in let (y,z):=x in y)).
  pose (c:=(λ t:I, let (_,x):=t in let (y,z):=x in z)).
  eapply tup_closed;[|apply (tsep_S_inf a b c)].
  auto_meet_intro_elim.
  now instantiate (3:=(a0,(a1,a2))).
Qed.


Lemma tsep_S_aux :
  let I:=prod X (prod X X) in
  let a:=(λ t:I, let (x,_):=t in x) in 
  let b:=(λ t:I, let (_,x):=t in let (y,z):=x in y) in
  let c:=(λ t:I, let (_,x):=t in let (y,z):=x in z) in
  ⋂[ i] ⊓ (λ x0 : X, ∃ c0 : X, x0 = (¬ (((a i ⊗ b i) ⊗ c i) ⊗ ¬ c0))                                                 ∧ a i @⊗ c i ≤ b i @⊗ c i ⊗↦ c0) ∈ʆ
  → (λ⊗ (λ x , λ⊗ (λ y,  λ⊗ (λ z, (x @⊗ z) @⊗ (y@⊗z))))) ∈ʆ.
Proof.
  intros.
  unfold tabs,tarrow.
  apply tup_closed with (⋂[ a] (¬ (a ⊗ ¬ ¬ (⋃[ a1] (a1 ⊗ ¬ ⋂[ a2] (¬ (a2 ⊗ ¬ (a @⊗ a2) @⊗ (a1 @⊗ a2)))))))).
  1: {  auto_meet_intro_elim.
        apply tneg_mon,tensor_mon_r.
        rewrite tneg_join.
        apply tneg_mon.
        auto_meet_intro. auto_meet_elim_risky.
        later;reflexivity.
  }
  auto_dni_tensor_r.
  apply tup_closed with (⋂[ i] ⋂[a1] (¬ (i ⊗ (a1 ⊗ ¬ ⋂[ a2] (¬ (a2 ⊗ ¬ i @⊗ a2 @⊗ (a1 @⊗ a2))))))).
  auto_meet_intro.
  apply ord_trans with (⋂[ a1] (¬ (a0 ⊗ a1 ⊗ ¬ ⋂[ a2] (¬ (a2 ⊗ ¬ a0 @⊗ a2 @⊗ (a1 @⊗ a2))))));[apply meet_elim;later;reflexivity|].
  rewrite tensor_join_r,tneg_join.
  auto_meet_intro. intros x0 (?b,((?z,((?z,?H),?H)),?H)). subst.
  apply meet_elim;later;reflexivity.
  apply tup_closed with (⋂[ i](¬ (a i ⊗ b i ⊗ ¬ ⋂[ a2] (¬ (a2 ⊗ ¬ a i @⊗ a2 @⊗ (b i @⊗ a2)))))).
  1:{ auto_meet_intro_elim. now instantiate (3:=(a0,(a1,a1))). }
  apply tens_assoc_r_sep_inf. 
  apply tup_closed with ( ⋂[ i] (¬ ((a i ⊗ b i) ⊗ ¬ ¬ ⋃[ a2] (a2 ⊗ ¬ a i @⊗ a2 @⊗ (b i @⊗ a2))))).
  1: { auto_meet_intro_elim.  apply tneg_mon,tensor_mon_r.
       rewrite tneg_join. apply tneg_mon. unfold image_set. auto_meet_intro_elim.
       auto_meet_elim. later;split;[|reflexivity]. later;reflexivity.
  }
  auto_dni_tensor_r.
   apply tup_closed with  (⋂[ i] ⋂[a2] (¬ ((a i ⊗ b i) ⊗ (a2 ⊗ ¬ a i @⊗ a2 @⊗ (b i @⊗ a2))))).
  1: {  auto_meet_intro. rename a0 into i.
  apply ord_trans with ( ⋂[a2] (¬ ((a i ⊗ b i) ⊗ (a2 ⊗ ¬ a i @⊗ a2 @⊗ (b i @⊗ a2)))));[apply meet_elim;later;reflexivity|].
  rewrite tensor_join_r,tneg_join.
  auto_meet_intro. intros x0 (?b,((?z,((?z,?H),?H)),?H)). subst.
  apply meet_elim;later;reflexivity.
  }
  apply tup_closed with (⋂[ i] (¬ ((a i ⊗ b i) ⊗ c i ⊗ ¬ a i @⊗ c i @⊗ (b i @⊗ c i)))).
  1: {  repeat auto_meet_intro. destruct a0 as (c0,(c1,c2)). simpl.
        auto_meet_elim_risky. instantiate (3:=(c0,(c1,a1))). reflexivity. }
  apply tens_assoc_r_sep_inf.
  unfold tapp at 1.
  apply tup_closed 
    with (⋂[ i] (¬ (((a i ⊗ b i) ⊗ c i )⊗ (¬ ⊓ (image_set tneg (λ x0:X, ∃ c0:X, x0 = (¬ c0)
                                              ∧ a i @⊗ c i ≤ b i @⊗ c i ⊗↦ c0)))))).
  1: { auto_meet_intro_elim. instantiate (3:=a0).
       repeat (try (apply tneg_mon);try (apply tensor_mon_r)).
       auto_meet_intro_elim. subst. auto_meet_elim.
    exists (¬ a1);split;[later;split;[reflexivity|assumption]|reflexivity].
  }
  apply tup_closed with 
      (⋂[ i] (¬ (((a i ⊗ b i) ⊗ c i) ⊗
     ¬ ¬ (join_set (λ x0 : X, ∃ c0 : X, x0 = (¬ c0) ∧ a i @⊗ c i ≤ b i @⊗ c i ⊗↦ c0))))).
  1: { auto_meet_intro_elim. instantiate (3:=a0).
       repeat (try (apply tneg_mon);try (apply tensor_mon_r)).
       now rewrite tneg_join. }
  auto_dni_tensor_r.
  eapply tup_closed  
    with (⋂[ i] meet_set (λ x0:X, ∃ c0, x0= tneg (((a i ⊗ b i) ⊗ c i) ⊗ ¬ c0)
                                        ∧ a i @⊗ c i ≤ b i @⊗ c i ⊗↦ c0)).
  2: assumption.
  auto_meet_intro. rewrite tensor_join_r,tneg_join.
  auto_meet_intro_elim. intros ?y (?y,((?y,((?y,(?H,?H)),?H)),?H)). subst.
  auto_meet_elim_trans. instantiate (3:=a0). auto_meet_elim.
  later;split;auto.
Qed.



Lemma inf_sigma `{I:Type} (a b c:I→X) :
  let J:={j:(I*X) & let (i,d):=j in a i @⊗ c i ≤ b i @⊗ c i ⊗↦ d} in
  let a':=(λ t:J, let (x,_):=t in let (y,_):=x in a y) in
  let b':=(λ t:J, let (x,_):=t in let (y,_):=x in b y) in
  let c':=(λ t:J, let (x,_):=t in let (y,_):=x in c y) in
  let d':=(λ t:J, let (x,_):=t in let (_,z):=x in z) in
  ⋂[ i] ⊓ (λ x0 : X, ∃ c0 : X, x0 = (¬ (((a i ⊗ b i) ⊗ c i) ⊗ ¬ c0))
                        ∧ a i @⊗ c i ≤ b i @⊗ c i ⊗↦ c0)
  =  ⋂[ i] (¬ (((a' i ⊗ b' i) ⊗ c' i) ⊗ ¬ (d' i))).
Proof.
  apply ord_antisym.
  -  auto_meet_intro.
     destruct a0 as ((i,d),Hi).
     auto_meet_elim_trans.
     auto_meet_elim.
     exists d. split;[reflexivity|exact Hi].
  -  repeat auto_meet_intro.
     auto_meet_elim.
     exists (@existT (I * X) _ (a0,a1) H1).
     reflexivity.
Qed.

Lemma S_true_aux `{I:Type} (a b c:I→X) :
  let J:={j:(I*X) & let (i,d):=j in a i @⊗ c i ≤ b i @⊗ c i ⊗↦ d} in
  let a':=(λ t:J, let (x,_):=t in let (y,_):=x in a y) in
  let b':=(λ t:J, let (x,_):=t in let (y,_):=x in b y) in
  let c':=(λ t:J, let (x,_):=t in let (y,_):=x in c y) in
  let d':=(λ t:J, let (x,_):=t in let (_,z):=x in z) in
  ( ⋂[ i] ((¬ ((((c' i ⊗↦ (b' i @⊗ c' i) ⊗↦ d'   i) ⊗ b' i) ⊗ c' i) ⊗ ¬ (d' i)))) ∈ʆ) 
 → 
  ( ⋂[ i] ⊓ (λ x0 : X, ∃ c0 : X, x0 = (¬ (((a i ⊗ b i) ⊗ c i) ⊗ ¬ c0))                                                 ∧ a i @⊗ c i ≤ b i @⊗ c i ⊗↦ c0) ∈ʆ).
Proof.
  intros.
  rewrite inf_sigma.
  repeat rewrite <- tens_assoc_sep_inf.
  eapply C6n_l_inf with (λ i, c' i ⊗↦ b' i @⊗ c' i ⊗↦ d' i).
  - repeat rewrite tens_assoc_sep_inf.
    eapply tup_closed;[|exact H].
    auto_meet_intro.
    destruct a0 as ((i,d),p). simpl.
    apply meet_elim.
    now exists (existT _ (i,d) p). 
  -  eapply tup_closed;[|apply app_entails_aux]. 
    auto_meet_intro. clear H0.
    destruct a0 as ((i,d),p). simpl.
    auto_meet_elim_trans.
    do 2 (apply tarrow_mon_r).
    assumption.
Qed.

Lemma S_true_bis `{I:Type} (a' b' c' d':I→X) :
   ( ⋂[ i] (((((c' i ⊗↦ (b' i @⊗ c' i) ⊗↦ d' i)) ⊗↦ (c' i ⊗↦ (b' i @⊗ c' i)) ⊗↦ c' i ⊗↦ (d' i)))) ∈ʆ)
   →
   ( ⋂[ i] ((¬ ((((c' i ⊗↦ (b' i @⊗ c' i) ⊗↦ d' i) ⊗ b' i) ⊗ c' i) ⊗ ¬ (d' i)))) ∈ʆ).
Proof.
  intros H.
  do 2 (apply tens_assoc_l_sep_inf).
  do 1 (apply tens_comm_sep_inf).
  repeat (apply tens_assoc_l_sep_inf).
  eapply C6n_l_inf;[|apply (app_entails_inf _ c')].
  simpl.
  apply tens_assoc_r_sep_inf.
  apply tens_comm_sep_inf.
  apply tens_assoc_r_sep_inf; auto_dne_tensor_r.
  apply tens_assoc_l_sep_inf; auto_dne_tensor_r.
  eapply tup_closed;[|exact H].
  auto_meet_intro_elim.
  unfold tarrow.
  reflexivity.
Qed.


Lemma preS_true `{I:Type} (a b c:I→X) :
  let J:={j:(I*X) & let (i,d):=j in a i @⊗ c i ≤ b i @⊗ c i ⊗↦ d} in
  let a':=(λ t:J, let (x,_):=t in let (y,_):=x in a y) in
  let b':=(λ t:J, let (x,_):=t in let (y,_):=x in b y) in
  let c':=(λ t:J, let (x,_):=t in let (y,_):=x in c y) in
  let d':=(λ t:J, let (x,_):=t in let (_,z):=x in z) in
   ( ⋂[ i] (((((c' i ⊗↦ (b' i @⊗ c' i) ⊗↦ d' i)) ⊗↦ (c' i ⊗↦ (b' i @⊗ c' i)) ⊗↦ c' i ⊗↦ (d' i)))) ∈ʆ)
  →
  ( ⋂[ i] ⊓ (λ x0 : X, ∃ c0 : X, x0 = (¬ (((a i ⊗ b i) ⊗ c i) ⊗ ¬ c0))                                                 ∧ a i @⊗ c i ≤ b i @⊗ c i ⊗↦ c0) ∈ʆ).
Proof.
  intros.
  apply S_true_aux.
  eapply (S_true_bis a' b' c' d').
  exact H.
Qed.

Lemma tsep_S: (λ⊗ (λ x , λ⊗ (λ y,  λ⊗ (λ z, (x @⊗ z) @⊗ (y@⊗z))))) ∈ʆ.
Proof.
  pose (I:=prod X (prod X X)).
  pose (a:=(λ t:I, let (x,_):=t in x)).
  pose (b:=(λ t:I, let (_,x):=t in let (y,z):=x in y)).
  pose (c:=(λ t:I, let (_,x):=t in let (y,z):=x in z)).
  enough ( ⋂[ i] ⊓ (λ x0 : X, ∃ c0 : X, x0 = (¬ (((a i ⊗ b i) ⊗ c i) ⊗ ¬ c0))                                                 ∧ a i @⊗ c i ≤ b i @⊗ c i ⊗↦ c0) ∈ʆ).
  apply tsep_S_aux,H.
  apply preS_true.
  eapply tup_closed;[|apply tsep_S_type].
  auto_meet_intro.
  pose (j:=a0).
  destruct a0 as ((i,d),p). simpl.
  repeat auto_meet_elim_trans.
Qed.  

(** *** λ-calculus *)

Lemma lambda_adequate (u:X→X) (a b:X): (∀ x:X, x≤ a → u x ≤ b) → (λ⊗ u) ≤ a ⊗↦ b.
Proof.
  intros H.
  apply tadj.
  unfold antiapp.
  apply join_elim.
  eexists;split;[reflexivity|].
  apply meet_elim_trans.
  exists (a⊗↦ u a). split.
  eexists;intuition.
  apply tarrow_mon_r.
  now apply H.
Qed.


Lemma app_adequate (t u a b:X): t≤ a ⊗↦b → u ≤ a → t@⊗u ≤ ¬¬ b.
Proof.
  intros Ht Hu.
  apply demiadj_app.
  apply ord_trans with (a⊗↦b);intuition.
  now  apply tarrow_mon_l.
Qed.


Lemma beta_reduction (f :X→X) (a:X): (λ⊗ f)@⊗ a ≤ ¬¬ (f a).
Proof.
  apply demiadj_app. unfold tabs,tarrow.
  auto_meet_elim_trans.
Qed.



(** ** Tripos construction *)

(** We prove here a few of the properties necessary to the construction
of a tripos. *)

Lemma tauto a: a ∈ʆ <-> ∃s, s∈ʆ ∧ s≤a.
Proof.
split;intros.
- exists a;intuition.
- destruct H as (s,(Hs,Ha)). apply tup_closed with s; intuition.
Qed.

Lemma tauto_inf `{I:Set} (a:I→X): (⋂[i] a i) ∈ʆ <-> ∃s, s∈ʆ ∧ ∀i, s≤a i.
Proof.
split;intros.
- apply tauto in H. destruct H as (s,(Sep,Hs)).
  exists s;intuition . transitivity (⋂[i] a i);intuition.
  auto_meet_elim_risky.
- apply tauto. destruct H as (s,(Sep,Hs)).
  exists s;intuition.
 auto_meet_intro;subst;intuition.
Qed.

Lemma tauto_2inf `{I:Set} `{J:Set} (a:I→J→X): (⋂[i] ⋂[j]  a i j) ∈ʆ <-> ∃s, s∈ʆ ∧ ∀i j, s≤a i j.
Proof.
split;intros.
- apply tauto_inf in H. destruct H as (s,(Sep,Hs)).
  exists s;intuition . transitivity (⋂[j] a i j);intuition.
  auto_meet_elim_risky.
- apply tauto. destruct H as (s,(Sep,Hs)).
  exists s;intuition.
 do 2 (auto_meet_intro;subst;intuition).
Qed.


(** The interpretation of the forall quantifier 
   ∀j: IxJ  →   I 
       b_ij ↦ ¬⋃[j]{¬ b_ij }
defines a right adjoint to the image of the projection T(π):I→IxJ .*)


Lemma tripos_fa1 `{I:Set} `{J:Set} (a: I → X) (b:I→J→X):
( ∃ s, s∈ʆ ∧ ∀i j, s≤ a i ⊗↦ b i j ) → ∃ s, s∈ʆ ∧ ∀ i, s ≤ a i ⊗↦ (¬ (⋃[j] ¬ (b i j))).
Proof.
intros H.
apply tauto_2inf in H.
apply tauto_inf.
apply S4d_n_inf with (λ i, ⋃[j] (¬ b i j)).
apply dne_t_inf.
replace (⋂[ i] (¬ (a i ⊗ ⋃[ j] (¬ b i j)))) with (⋂[ i] ⋂[j](¬ (a i ⊗  (¬ b i j)))).
- intuition.
- apply meet_same_set;split;unfold Included,In;
  intros;destruct H0 as (i,H');subst;exists i;
   rewrite recognize_img_neg, <- tneg_join, tensor_join_r,<- recognize_img_tensor_r;
   reflexivity.
Qed.

Lemma tripos_fa2 `{I:Set} `{J:Set} (a: I → X) (b:I→J→X):
(∃ s, s∈ʆ ∧ ∀ i, s ≤ a i ⊗↦ (¬ (⋃[j] ¬ (b i j)))) → ( ∃ s, s∈ʆ ∧ ∀i j, s≤ a i ⊗↦ b i j ).
Proof.
intros H.
apply tauto_inf in H.
apply tauto_2inf.
replace (⋂[ i] (⋂[ j] (a i ⊗↦ b i j))) with (⋂[ i] (¬ (a i ⊗ ⋃[ j] (¬ b i j)))).
- apply S4d_n_inf with (λ i, ¬¬(⋃[j] (¬ b i j))).
  apply dni_t_inf. intuition.
- apply meet_same_set;split;unfold Included,In;
  intros;destruct H0 as (i,H');subst;exists i;
 rewrite tensor_join_r, <- recognize_img_tensor_r, tneg_join, <-recognize_img_neg;
   reflexivity.
Qed.


(** The interpretation of the existential quantifier 
   ∃j: IxJ  →   I 
       b_ij ↦ ⋃[j] b_ij 
defines a left adjoint to the image of the projection T(π):I→IxJ .*)



Lemma tripos_ex1 `{I:Set} `{J:Set} (a: I → J → X) (b:I→X):
( ∃ s, s∈ʆ ∧ ∀i j, s≤ a i j ⊗↦ b i  ) → ∃ s, s∈ʆ ∧ ∀ i, s ≤  ( ⋃[j] a i j) ⊗↦ b i .
Proof.
intros H.
apply tauto_2inf in H.
apply tauto_inf.
replace (⋂[ i] ((⋃[ j] a i j) ⊗↦ b i)) with (⋂[ i] (⋂[ j] (a i j ⊗↦ b i))).
- intuition.
- apply meet_same_set;split;unfold Included,In;
  intros;destruct H0 as (i,H');subst;exists i;
  rewrite tarrow_join;
  apply meet_same_set;split;unfold Included,In;
  intros;destruct H0 as (j,H');subst.
  * exists (a i j);split;[exists j|];reflexivity.
  * destruct H' as ((z,Hz),H'). subst;eexists;reflexivity. 
  * destruct H' as ((z,Hz),H'). subst;eexists;reflexivity.
  * exists (a i j);split;[exists j|];reflexivity.  
Qed.

Lemma tripos_ex2 `{I:Set} `{J:Set}  (a: I → J → X) (b:I→X):
(∃ s, s∈ʆ ∧ ∀ i, s ≤  ( ⋃[j] a i j) ⊗↦ b i) → ( ∃ s, s∈ʆ ∧ ∀i j, s≤ a i j ⊗↦ b i  ) .
Proof.
intros H.
apply tauto_inf in H.
apply tauto_2inf.
replace (⋂[ i] ((⋃[ j] a i j) ⊗↦ b i)) with (⋂[ i] (⋂[ j] (a i j ⊗↦ b i))) in H.
intuition.
- apply meet_same_set;split;unfold Included,In;
  intros;destruct H0 as (i,H');subst;exists i;
  rewrite tarrow_join;
  apply meet_same_set;split;unfold Included,In;
  intros;destruct H0 as (j,H');subst.
  * exists (a i j);split;[exists j|];reflexivity.
  * destruct H' as ((z,Hz),H'). subst;eexists;reflexivity. 
  * destruct H' as ((z,Hz),H'). subst;eexists;reflexivity.
  * exists (a i j);split;[exists j|];reflexivity.  
Qed.



(** We check that if for any i, s ≤ T ⊗↦ a i i,
then for all i,j  we have for λz.z(s (λx.x)≤ [i=j] ⊗↦ ¬¬¬¬ a i j.
In particular, this implies that ⋂[i,j] ([i=j] ⊗↦ ¬¬¬¬ a i j)∈ʆ
and using C6_t_inf and dne_t_inf twice, it entails that ⋂[i,j] ([i=j] ⊗↦ a i j)∈ʆ *)

Lemma tripos_eq1 `{I:Set}  (a: I → I → X) s:
 s∈ʆ → (∀ i, s ≤  top ⊗↦ a i i)
 → ∃ t, t=( λ⊗ (λ x, x @⊗ (s @⊗ λ⊗ (λ x,x)))) 
      ∧ ∀ i j, ( t ≤ (((top ⊗↦ ⊥) ⊗↦ (¬¬¬¬ a i j))) 
            ∧  ( t ≤ ((⋂[a](a⊗↦a)) ⊗↦ ¬¬ ¬¬ a i i))) .
Proof.
  intros Sep Hs;
  eexists;split;[reflexivity|].
intros   i;split.
  - apply meet_elim_trans.
  eexists;split;[exists ( ⊤ ⊗↦ ⊥);reflexivity|].
  apply tarrow_mon_r.
  apply demiadj_app.
  apply tarrow_mon;intuition.
  - apply meet_elim_trans.
  eexists;split;[exists ( ⋂[ a0] (a0 ⊗↦ a0));reflexivity|].
  apply tarrow_mon_r.
  apply demiadj_app.
  apply meet_elim_trans.
  eexists;split;[exists (s@⊗( ⋂[ a0] (a0 ⊗↦ a0)));reflexivity|].
  apply tarrow_mon_r. 
  apply demiadj_app.
  eapply ord_trans;[apply (Hs i)|].  
  apply tarrow_mon_l;intuition.
Qed.

Lemma tripos_eq2 `{I:Set}  (a: I → I → X):
 (∀ i,(⋂[a](a⊗↦a)  ⊢⊗ a i i))
 → ∀i,(top ⊢⊗ a i i) .
Proof.
  intros H i.
  specialize (H i).
  apply C6_t  with (⋂[a](a⊗↦a));intuition.
  apply tmodus_ponens with (⋂[a](a⊗↦a)).
  - exact (id_t_inf (λ x,x)).
  - remember (⋂[a](a⊗↦a)) as Id.
  apply tens_assoc_sep.
    apply S2_t.
Qed.



(** Last, we check that we indeed have a structure
of Heyting algebra induced by ⊗ and ⊗↦ . *)
    
Definition tor a b := ¬((¬a)⊗¬b).
Definition tand a b := (a⊗b).
Notation "a + b":= (tor a b).
Notation "a × b":= (tand a b) (at level 74).
Notation "a ≤⊗ b":= (entails a b) (at level 75).


Lemma Heyting_adj a b c: a ≤⊗ b ⊗↦c <-> (a×b) ≤⊗ c.
Proof.
  split;unfold entails,tarrow;intro H.
  - apply tens_assoc_sep.
    now under_arrow_r dni_t.
  - under_arrow_r dne_t.
    now apply tens_assoc_sep.
Qed.

(** ⊗ defines a meet *)
Lemma Heyting_and_l a b: a×b ≤⊗ a .
Proof.
  apply S2_t.
Qed.

Lemma Heyting_and_r a b: a×b ≤⊗ b .
Proof.
  under_arrow_l S3_t.
  apply Heyting_and_l.
Qed.


(** This is coherent with the lattice structure *)
Lemma is_lattice a b: a ≤⊗ b → a ≤⊗ a × b.
Proof.
  intros H.
  apply C6_t with (a⊗a).
  apply S1_t.
  now apply S4_t.
Qed.  


(** + defines a join *)

Lemma Heyting_or_l a b: a ≤⊗ a+b .
Proof.
  under_arrow_r dne_t.
  apply tens_assoc_sep.
  apply extra_hyp_r.
  apply id_t.
Qed.

Lemma Heyting_or_r a b: b ≤⊗ a+b .
Proof.
  under_arrow_r dne_t.
  under_arrow_r S3_t.
  under_arrow_r dni_t.
  apply Heyting_or_l.
Qed.

End About_TensorAlg.





(** ** Classical conjunctive algebras *)

Section About_KTensorAlg.
Context `{KTA:KTensorAlgebra}.
Notation "¬ a":=(tneg a).
Notation "a ⊢⊗ b" := (entails a b) (at level 75). 
Notation "a ∈ʆ":=(tsep a).


(** We have the usual modus ponens. *)

Lemma MP_t a b: (a ⊢⊗ b) → a ∈ʆ → b ∈ʆ.
Proof.
  intros Hab Ha.
  apply tdne.
  apply tmodus_ponens with a;assumption.
Qed.



Lemma explosive a: a∈ʆ → (¬a)∈ʆ → ⊥ ∈ʆ.
Proof.
  intros Ha Hna.
  apply MP_t with a;intuition.
  apply MP_t with (¬ a);intuition.
  apply tneg_imp_bot.
Qed.

End About_KTensorAlg.


