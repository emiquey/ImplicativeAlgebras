Set Implicit Arguments.
Set Contextual Implicit.

Require Import Utf8.
Require Import Morphisms.
Require Import Sets.Ensembles.
Require Import ImpAlg.Lattices.
Require Import TLC.LibLN.


(** We define the lifting of the arrow (a ↦ b) for sets (A ↦ b, a ↦ B, A ↦ B) *)

Infix "≅" := same_set (at level 69).

Definition arrow_set_l `{X:Type} `{arrow:X → X → X} (A:Ens) (b:X):Ens :=
  fun x => ∃a, A a ∧ x = arrow a  b.
Definition arrow_set_r `{X:Type} `{arrow:X → X → X} (a:X) (B:Ens):Ens :=
    fun x => ∃b, B b ∧ x = arrow a b.
Definition arrow_set `{X:Type} `{arrow:X → X → X} (A:Ens) (B:Ens):Ens :=
    fun x => ∃a,∃b, A a ∧ B b ∧ x = arrow a b.

Global Instance arrow_set_Proper `{X:Type} `{arrow:X → X → X} a:
  Proper ((@same_set X) ==> (@same_set X)) ((@arrow_set_r X arrow) a).
Proof.
  intros A B HAB.
  split;intros x Hx; destruct Hx as [b [Hb Hx]];
  exists b;
  split;[apply HAB,Hb| assumption|apply HAB,Hb| assumption].
Qed.




Definition not_empty `{X:Type} A:= ∃ x:X, A x.

(** Les deux axiomes de logiques nécessaires au développement:
- un ensemble est soit vide soit habité
- si f est une fonction partielle de X dans A, pour tout x:X, f x est soit définie soit non
 *)


Axiom empty_dec: ∀X:Type, ∀A:Ens, {@not_empty X A} + {∀x, ¬ A x }.
Axiom option_dec : ∀ X:Type, ∀ a:option X, {a = None} + {∃d, a =  Some d}.


(** * QuasiImplicative Structures *)

(** ** Definition *)
Class QuasiImplicativeStructure `{CL:CompleteLattice} :=
  {
    arrow:X → X → X;

    arrow_mon_l: ∀ a a' b, ord a a' ->  ord (arrow a' b ) (arrow a b );
    arrow_mon_r: ∀ a b b', ord b b' ->  ord (arrow a b  ) (arrow a b');

    arrow_meet: ∀ a B, not_empty B → arrow a (meet_set B) = meet_set (@arrow_set_r _ arrow a B)
  }.

Infix "↦" := arrow (at level 60, right associativity):ia_scope.
Notation "a ↪ B":= (@arrow_set_r _ _ a B) (at level 69):ia_scope.




Generalizable Variables X ord join meet arrow L meet_set join_set.

(** ** Properties of implicative structures *)

(** Variance of the arrow *)
Lemma arrow_mon `{QIS:QuasiImplicativeStructure}: ∀ a a' b b' , a' ≤ a -> b ≤ b' -> arrow a b ≤ arrow a' b'.
Proof.
  intros.
  apply (ord_trans  _ (a' ↦ b)).
  - apply arrow_mon_l; auto.
  - apply arrow_mon_r; auto.
Qed.


(** We can always express set defined with an arrow in prenex form. *)
Lemma arrow_prenex `{QIS:QuasiImplicativeStructure}:
  ∀ a f, (λ y:X, ∃ b : X, y = a ↦ f a b) ≅ (@arrow_set_r X arrow a  (λ y : X, ∃ b : X, y = f a b)).
Proof.
  intros.
  unfold arrow_set_l.
  split;intros x [b Hb].
  - exists (f a b).
    split;[exists b | rewrite Hb];reflexivity.
  - destruct Hb as [Hb Hy].
    destruct Hb as [c Hc].
    exists c;rewrite Hy,Hc;reflexivity.
Qed.

 

(** * Encoding the λ-calculus         *)


(** ** Application *)
(** We begin by defining the application of "a" to "b", 
     denoted by "a@b". *)


Definition part_fun `{A:Type} `{B:Type}:= A → option B.
Definition domain  `{A:Type} `{B:Type} (f:@part_fun A B):= fun a => ∃ b, f a = Some b.



Definition omeet_set `{QIS:QuasiImplicativeStructure} (A:Ens) :=
  If (not_empty A) then Some (meet_set A) else None.

Inductive partial_meet_set `{QIS:QuasiImplicativeStructure} (A:Ens): X → Prop :=
  | meet_some: not_empty A → partial_meet_set A (meet_set A).

(** L'application est définie de façon **partielle** *)

Definition U `{QIS:QuasiImplicativeStructure} a b:=(fun x => a ≤ b ↦ x).
Definition oapp `{QIS:QuasiImplicativeStructure} (a b:X):=omeet_set (U a b).

Infix "@" := oapp (at level 59, left associativity).


Inductive ord_option `{O:Order}: option X → option X → Prop:=
  |ord_some: ∀a b, a ≤ b → ord_option (Some a) (Some b).

Notation "a ≤o b":=(ord_option a b) (at level 75).

Lemma oord_trans `{O:Order} a b c: a ≤o b → b ≤o c → a ≤o c.
Proof.
  intros Ha Hb.
  inversion Ha. inversion Hb.
  rewrite <- H1 in H3.
  inversion H3;subst.
  apply ord_some.
  apply ord_trans with b0;assumption.
Qed.  

Definition is_some `{X:Type} (a:option X) := ∃ x,a=Some x.

Ltac auto_some_step :=
  match goal with
  |[H:is_some _ |- _ ] => destruct H as [?x ?H];rewrite ?H in *
  |[ |- (Some _) ≤o (Some _) ] => apply ord_some
  end.                                                

Ltac auto_some := repeat auto_some_step.



(** Monotonicity of the application *)

Lemma is_some_oapp `{QIS:QuasiImplicativeStructure}:
  ∀ a b, is_some (a@b) <-> a@b = Some (meet_set (U a b)).
Proof.
  intros a b;split;intros H.
  - destruct H. unfold oapp,omeet_set in *.
    destruct (@empty_dec X (U a b)).
    now rewrite If_l.
    rewrite If_r in H.
    inversion H.
    intros (c,Hc).
    now apply (n c).
  - rewrite H.
    later;reflexivity.
Qed.     

Lemma some_oapp `{QIS:QuasiImplicativeStructure}:
  ∀ a b c,  a @ b = Some c → c = meet_set (λ x, a ≤ b ↦ x).
Proof.
  intros a b c H.
  unfold oapp,omeet_set,U in *.
  rewrite If_l in H;inversion H;auto.
  destruct (@empty_dec X (U a b));auto.
  rewrite If_r in H;inversion H;auto.
  intros (d,Hd).
  now apply (n d).
Qed.

Lemma some_not_empty `{QIS:QuasiImplicativeStructure}:
  ∀ a b c,  a @ b = Some c → not_empty (λ x, a ≤ b ↦ x).
Proof.
  intros a b c H.
  unfold oapp,omeet_set,U in *.
  destruct (@empty_dec X (U a b));auto.
  rewrite If_r in H;inversion H;auto.
  intros (d,Hd).
  now apply (n d).
Qed.


Lemma app_mon_l `{QIS:QuasiImplicativeStructure}:
  ∀ (a b c:X), a ≤ b →  is_some (a@c) → is_some (b@c) →  a@c ≤o b@c.
Proof.
  intros a b c Hab Sac Sbc.
  apply is_some_oapp in Sac.
  apply is_some_oapp in Sbc.
  rewrite Sac,Sbc.
  apply ord_some.
  unfold U in *.
  auto_meet_leq.
  intros x Hx.
  apply meet_elim.
  apply (ord_trans _ _ _  Hab Hx).
Qed.

Lemma app_mon_r `{QIS:QuasiImplicativeStructure}:
  ∀ a b c, b ≤ c  →  is_some (a@b) → is_some (a@c) → a @ b ≤o a @ c.
Proof.
  intros a b c Hbc Sab Sac.
  apply is_some_oapp in Sac.
  apply is_some_oapp in Sab.
  rewrite Sac,Sab.
  apply ord_some.
  unfold U in *.
  apply meet_subset.
  intros x Hx.
  apply (ord_trans _ _ _  Hx).
  now apply arrow_mon_l.
Qed.

Lemma app_mon `{QIS:QuasiImplicativeStructure}:
  ∀ a a' b b', is_some (a@b) → is_some (a'@b') → a≤ a' → b ≤ b' → a @ b ≤o a' @ b'.
Proof.
  intros a a' b b' Sab Sab' Ha Hb.
  eapply oord_trans;[apply (app_mon_l _ _ _ Ha)|];auto.
  apply is_some_oapp in Sab'.
  unfold oapp,omeet_set,U in *.
  rewrite If_l.
  later;reflexivity.
  destruct (@empty_dec _ (U a' b')) as [H| H];unfold U in * .
  - destruct H as (x,Hx).
    exists x. apply ord_trans with (b' ↦ x);auto.
    now apply arrow_mon_l.
  - rewrite If_r in Sab'. inversion Sab'.
    intros (y,Hy). apply (H y Hy).
  - apply (app_mon_r _ _ _ Hb).
    destruct (@empty_dec _ (U a' b)) as [H| H];unfold oapp,omeet_set,U in * .
    + destruct H as (x,Hx).
      rewrite If_l.
      later;reflexivity.
      exists x. auto.
    + rewrite If_r in Sab'.
      destruct Sab' as (z,Hz);inversion Hz.
      intros (x,Hz).
      apply (H x).
      apply ord_trans with (b' ↦ x);auto.
      now apply arrow_mon_l.
    + assumption.
Qed.



(** ß-reduction is reflected by the order. *)

Lemma app_beta `{QIS:QuasiImplicativeStructure}:
  ∀ a b, is_some ((a ↦ b) @ a) → (a ↦ b) @ a ≤o Some b.
Proof.
  intros a b H.
  apply is_some_oapp in H;unfold oapp,omeet_set,U in *.
  rewrite If_l;[|exists b;apply by_refl;reflexivity].
  auto_some.
  apply meet_elim.
  reflexivity.
Qed.



(** η-expansion is reflected by the reverse order. *)

Lemma app_eta `{QIS:QuasiImplicativeStructure}:
  ∀ a b c,  (a@b) = Some c → a ≤ b ↦ c.
Proof.
  intros a b c H.
  unfold oapp,omeet_set,U in H.
  assert (H1:=H).
  apply some_oapp in H;subst.
  apply some_not_empty in H1.
  rewrite~ arrow_meet.
  apply meet_intro.
  intros x (c,(Hx,Hc)).
  now subst x.
Qed.


(** The application is a minimum.  *)

Lemma app_min `{QIS:QuasiImplicativeStructure}:
  ∀ a b c, (a@b) = Some c →  min (fun x => a ≤ b ↦ x) c.
Proof.
  intros a b c H.
  unfold oapp,omeet_set,U in H.
  assert (H1:=H). assert (H2:=H).
  apply some_oapp in H;subst.
  apply some_not_empty in H1.
  split.
  - unfold lb,app. intros y Hy. now apply meet_elim. 
  - now apply app_eta. 
Qed.


(** We have a fundamental adjunction.*)

Lemma adjunction `{QIS:QuasiImplicativeStructure}:
  ∀ a b c d, (a@b) = Some d →  d ≤ c ↔ a ≤ b ↦ c.
Proof.
  intros.
  split;intros.
  - apply (ord_trans _ _ _ (app_eta a b H )).
    now apply arrow_mon_r.
  - apply some_oapp in H. subst.
    apply meet_elim.
    assumption.
Qed.



(** ** Abstraction *)

(** We now define the abstraction of a function "f",
     which we denote by "λ' f". *)

Definition abs `{QIS:QuasiImplicativeStructure} f:= ⊓ (fun x => ∃ a b, f a = Some b ∧ x = a ↦ b).
Notation "λ- f":= (abs f) (at level 60):ia_scope.



(** Monotonicity *)

Lemma abs_mon `{QIS:QuasiImplicativeStructure}:
   ∀ f g, Included _ (domain g) (domain f) →  (∀a b c, f a = Some b ∧ g a = Some c ∧ b ≤ c) → (λ- f) ≤ (λ- g).
Proof.
intros f g Dom H.
auto_meet_leq.
intros x [a [b [ Hg Hx]]].
apply meet_elim_trans.
destruct Dom with a. exists b;auto.
eexists;split.
- exists a. exists x0;split;auto.
- subst. rewrite meet_top_r. apply arrow_mon_r.
  apply (H a). 
Qed.


(** ß-reduction is reflected by the order. *)

Lemma betarule `{QIS:QuasiImplicativeStructure}:
  ∀ f a b c, f a = Some b → (λ- f) @ a = Some c → c ≤ b.
Proof.
  intros f a b c Hf Hc.
  apply some_oapp in Hc;subst.
  do 2 apply meet_elim.
  exists a; exists b; split;auto.
Qed.


Lemma beta_red `{QIS:QuasiImplicativeStructure}: 
  ∀ f t u v z, f t = Some v → (λ- f)@t = Some z → v ≤ u → z ≤ u.
Proof.
  intros f t u v z Hv Hf H.
  eapply ord_trans;[apply (betarule f t Hv Hf)|exact H]. 
Qed.



(** η-expansion is reflected by the reverse order. *)

Definition eta  `{QIS:QuasiImplicativeStructure} a := fun x => a @ x.
Lemma etarule `{QIS:QuasiImplicativeStructure}:
   ∀ a, (∀ x, not_empty (λ x0 : X, a ≤ x ↦ x0)) → a ≤ (λ- (eta a)).
Proof.
  intros a H.  unfold eta,app,abs.
  apply meet_intro.
  intros b (x,(c,(Hc,Hb)));
    inversion Hc;subst.
  now apply app_eta.
Qed.

  


(** ** Properties of closures *)

Definition upwards_closure `{QIS:QuasiImplicativeStructure} (A:Ens):= ∀ a b, a ≤ b → A a → A b .
Definition app_closure     `{QIS:QuasiImplicativeStructure} (A:Ens):= ∀ a b c, a@b = Some c → A a → A b → A c.
Definition mod_pon_closure `{QIS:QuasiImplicativeStructure} (A:Ens):= ∀ a b, A a → A (a↦b) → A b.

(** Remark  *)
Remark mod_pon_app `{QIS:QuasiImplicativeStructure}:
  ∀ A:Ens, (∀ a b, not_empty (λ x : X, a ≤ b ↦ x)) →
           upwards_closure A → (app_closure A <-> mod_pon_closure A).
Proof.
  intros A NE UpC. split;intros H a b Ha Hb.
  - assert (((a↦b)@a) = Some (⊓ (λ x, a ↦ b ≤ a ↦ x))).
    + unfold oapp,omeet_set,U. rewrite~ If_l.
    + apply (UpC ((⊓ (λ x, a ↦ b ≤ a ↦ x)))).
      * now apply meet_elim. (* now apply adjunction. *)
      * apply (H (a↦b) a _ H0 Hb Ha).
  - rename Ha into c.
    intros Aa Ab.
    apply (H b);intuition.
    apply (UpC a).
    + now apply (adjunction a b _  Hb).
    + assumption.
Qed.


