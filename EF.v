Set Implicit Arguments.
Set Contextual Implicit.

Require Import Utf8.
Require Import Setoid.
Require Import Morphisms.
Require Export Relation_Definitions.
Require Import TLC.LibLN.
Require Import Sets.Ensembles.
Require Import ImpAlg.Lattices.
Require Import ImpAlg.Powerset.
Require Import ImpAlg.ImplicativeStructures.
Require Import ImpAlg.ImplicativeAlgebras.
Require Import ImpAlg.Separator. 
Require Import ImpAlg.Lambda.
Require Import ImpAlg.Combinators.


(** We formalize Cohen, Mota and Tate's notion of evidenced frame and establish the connexion with
implicative algebras. *)

(** * Definition *)

Class EvidencedFrame `{Φ:Type} `{Evidence : Type} `{realizes: Evidence -> Φ -> Φ -> Prop}:=
{
  (* Reflexivity *)
  eid: Evidence;
  id_ax: forall (φ:Φ), realizes eid φ φ;

  (* Transitivity *)
  trans: Evidence -> Evidence -> Evidence;
  trans_ax: forall (φ1 φ2 φ3 : Φ) (e12 e23 : Evidence), realizes e12 φ1 φ2 -> realizes e23 φ2 φ3 -> realizes (trans e12 e23) φ1 φ3;

  (* Top *)
  φ_top : Φ;
  etrue : Evidence;
  true_ax : forall φ : Φ, realizes etrue φ φ_top;

  (* Bottom *)
  φ_bot : Φ;
  efalse : Evidence;
  false_ax : forall φ : Φ, realizes efalse φ_bot φ;

  (* Conjunction *)
  φ_and : Φ -> Φ -> Φ;
  epair : Evidence -> Evidence -> Evidence;
  efst : Evidence;
  esnd : Evidence;
  and_intro : forall (φ φ1 φ2 : Φ) (e1 e2 : Evidence), realizes e1 φ φ1 -> realizes e2 φ φ2 -> realizes (epair e1 e2) φ (φ_and φ1 φ2);
  and_elim1 : forall φ1 φ2 : Φ, realizes efst (φ_and φ1 φ2) φ1;
  and_elim2 : forall φ1 φ2 : Φ, realizes esnd (φ_and φ1 φ2) φ2;

  (* Disjunction *)
  φ_or : Φ -> Φ -> Φ;
  einl : Evidence;
  einr : Evidence;
  ematch : Evidence -> Evidence -> Evidence;
  or_intro1 : forall φ1 φ2 : Φ,  realizes einl φ1 (φ_or φ1 φ2);
  or_intro2 : forall φ1 φ2 : Φ,  realizes einr φ2 (φ_or φ1 φ2);
  or_elim : forall (φ1 φ2 φ : Φ) (e1 e2 : Evidence), realizes e1 φ1 φ -> realizes e2 φ2 φ -> realizes (ematch e1 e2) (φ_or φ1 φ2) φ;

  (* Implication *)
  φ_imp : Φ -> Φ -> Φ;
  elambda : Evidence -> Evidence;
  eeval : Evidence;
  imp_intro : forall (φ φ1 φ2 : Φ) (e : Evidence),  realizes e (φ_and φ φ1) φ2 -> realizes (elambda e) φ (φ_imp φ1 φ2);
  imp_elim : forall φ1 φ2 : Φ,  realizes eeval (φ_and (φ_imp φ1 φ2) φ1) φ2;
  
  (* Forall *)
  φ_forall : forall (I : Set), (I -> Φ) -> Φ;
  e_fai : Evidence -> Evidence;
  e_fae : Evidence;
  forall_intro : forall (I : Set) (φ : Φ) (φi : I -> Φ) (e : Evidence), (forall i : I, realizes e φ (φi i)) -> realizes (e_fai e) φ  (φ_forall φi);
  forall_elim : forall (I : Set) (φi : I -> Φ) (i : I), realizes e_fae (φ_forall φi)  (φi i);
  
  (* Exists *)
  φ_exists : forall (I : Set), (I -> Φ) -> Φ;
  e_exi : Evidence;
  e_exe : Evidence -> Evidence;
  exists_intro : forall (I : Set) (φi : I -> Φ) (i : I), realizes e_exi (φi i) (φ_exists φi);
  exists_elim : forall (I : Set) (φi : I -> Φ) (φ : Φ) (e : Evidence), (forall i : I, realizes  e (φi i) φ) -> realizes (e_exe e) (φ_exists φi)  φ
                                                                                                            
}.



Definition fam2 `{Φ:Type} (φ1 φ2:Φ) : bool -> Φ:=
  fun b => If b=true then φ1 else φ2.
Transparent fam2.

Definition meaningless `{EF:EvidencedFrame}:=
  exists e,  forall φ1 φ2, realizes e (φ_and φ1 φ2) (φ_forall (fam2 φ1 φ2)).

Definition consistent `{EF:EvidencedFrame}:=
  not (∃ e, realizes e φ_top φ_bot).


(** * Evidenced Frame induced by Implicative Algebra *)

Section IA_to_EF.
  Context `{IA:ImplicativeAlgebra}.

  (** ** Structure *)
  
  Definition Φ:=X.
  Definition Evidence:= {x :X | separator x }.
  Definition GetEvidence:Evidence -> X:=fun x => proj1_sig x. 
  Coercion GetEvidence: Evidence >-> X.
  Notation "ι( x )":=(GetEvidence x).
  
  Definition realizes: Evidence -> Φ -> Φ -> Prop:=
    fun e φ θ => ι(e) ≤ φ ↦ θ.

  
  Notation "e ⊩ φ1 ⇒ φ2":=(realizes e φ1 φ2) (at level 50).

  Lemma beta_sep: forall f a, (λ- f) ∈ʆ → a∈ʆ → f a ∈ʆ.
  Proof.
    intros f a Hf Ha.
    eapply up_closed.
    apply betarule.
    apply app_closed;assumption.
  Qed.

  (** Notations from IAs *)

  Notation "λ+ t":= (trm_abs (t)) (at level 69, right associativity).
  Notation "[ t ] u":= (trm_app (t) u ) (at level 69, right associativity).
  Notation "$ n":= (@trm_bvar X n) (at level 69, right associativity).

  Notation "a × b":=(prodt a b) (at level 69).
  Notation "a ⊕ b":=(sumt a b) (at level 69).
  Notation "[∀] a":=(forallt a) (at level 69).
  Notation "[∃] a":=(existst a) (at level 69).

  
  (** Tactics *)

  (** We define a bunch of tactics which we hide from the doc 
      to ease readability.*)

  (* begin hide *)
  Ltac autoev:=
    repeat(
        match goal with
          | x:Evidence |- _ =>  destruct x as (x,?H)
        end
      );simpl.

    
  (** We define again the tactis to pick fresh variables.*)
  
  Definition env:=@env X.
  Definition codom:=@codom X.
  Definition fv_trm:=@fv_trm X.
  Definition fv_typ:=@fv_typ X.
    
  Ltac gather_vars :=
    let A := gather_vars_with (fun x : vars => x) in
    let B := gather_vars_with (fun x : var => \{x}) in
    let C := gather_vars_with (fun x : env => (dom x)\u (codom x)) in
    let D := gather_vars_with (fun x : trm => fv_trm x) in
    let E := gather_vars_with (fun x : typ => fv_typ x) in
    constr:(A \u B \u C \u D \u E).
  
  Ltac pick_fresh Y :=
    let L := gather_vars in (pick_fresh_gen L Y).
  
 
  Tactic Notation "apply_fresh" constr(T) "as" ident(x) :=
    apply_fresh_base T gather_vars x.
  Tactic Notation "apply_fresh" "*" constr(T) "as" ident(x) :=
    apply_fresh T as x; auto_star.
  Tactic Notation "apply_fresh" constr(T) :=
    apply_fresh_base T gather_vars ltac_no_arg.
  Tactic Notation "apply_fresh" "*" constr(T) :=
    apply_fresh T; auto_star.

  Ltac fresh_term_abs :=
    let L0:=gather_vars in let L1 := beautify_fset L0 in
                           apply (@term_abs _ L1);intros.
  Ltac fresh_sterm_abs :=
    let L0:=gather_vars in let L1 := beautify_fset L0 in
                           apply (@sterm_abs _ L1);intros.
  
  Ltac fresh_typing_abs :=
    let L0:=gather_vars in let L1 := beautify_fset L0 in
                           apply (@typing_abs _ L1);intros.
  
  Ltac autosterm :=
    repeat (
(*      try (apply sterm_cc);*)try (apply sterm_var);try (fresh_sterm_abs);
      try (apply sterm_app);try (unfold Succ);try (case_nat);try (cbn)
    ).

 
  Ltac autofv := cbn;repeat (try rewrite union_empty_l;try rewrite union_empty_r);reflexivity.
  Ltac autosclosed:=split;[autosterm|autofv].
  Ltac translated_sclosed:=split;[find_translated|autosclosed].
  Ltac exists_sterm t:= exists t;translated_sclosed.
  Ltac autoclosed_typ:=unfold closed_typ;cbn;repeat (rewrite union_empty_l);auto.

  Ltac simplIf:=repeat (try (rewrite If_l;[|reflexivity]);try (rewrite If_r;[|auto])).

  Ltac in_sep_sterm t:= apply sep_term;exists t;split;[find_translated|autosclosed].
  Ltac emeet_elim:=apply (meet_elim_trans);later;split;[later;reflexivity|].
  Ltac hnform:=repeat (emeet_elim;apply arrow_mon_r);adjunction_mon.
  Ltac realizer t:= apply realizer_later;later;split;[in_sep_sterm t| hnform].
  Ltac realizer_step t:= apply realizer_later;later;split;[in_sep_sterm t|].

  (* end hide *)
  
  (** ** Components *)

  (** *** Reflexivity *)
  Definition id:Evidence:=
    exist _ Id sep_Id.
  
  Lemma ax_id: forall (φ:Φ), id ⊩ φ ⇒ φ.
  Proof.
    intros φ.
    auto_meet_elim_trans.
  Qed.


  (** *** Transitivity *)
  Definition comp (a b:Evidence):Evidence.
    exists (λ- (fun (x:X) => ι(b) @(ι(a)@x))).
    autoev.
    apply (beta_sep (fun y => λ- (fun x => b@(y@x))) a);try assumption.
    apply (beta_sep (fun z => λ- (fun y => λ- (fun x => z@(y@x)))) b);try assumption.
    realizer (λ+ λ+ λ+ ([$2] ([$1] $0))).
  Defined.
    
  Lemma ax_comp: forall φ1 φ2 φ3 : Φ, forall e12 e23 : Evidence,
        e12 ⊩ φ1 ⇒ φ2  ->  e23 ⊩ φ2 ⇒ φ3  → (comp e12 e23) ⊩ φ1 ⇒ φ3.
  Proof.
    intros φ1 φ2 φ3 e12 e23 H12 H23.
    apply adjunction,beta_red,adjunction.
    transitivity (φ2 ↦ φ3).
    exact H23. 
    apply arrow_mon_l.
    now apply adjunction.
  Qed.

  
  (** *** Top *)
  Definition top_φ:Φ:=top.
  Definition tt:Evidence:=id.
  
  Lemma ax_true : forall φ : Φ, tt ⊩ φ ⇒ top_φ.
  Proof.
    intros φ.
    auto_meet_elim_trans.
    apply arrow_mon_r.
    apply top_max.
  Qed.

  (** *** Bottom *)
  Definition bot_φ : Φ:=bot.
  Definition ff : Evidence:=id.

  Lemma ax_false : forall φ : Φ, ff ⊩ bot_φ ⇒ φ.
  Proof.
    intros φ.
    auto_meet_elim_trans.
    apply arrow_mon_l.
    apply bot_min.
  Qed.
  
    
  (** *** Conjunction *)

  Definition and_φ := fun x y =>  x × y. 
  Definition paire:Evidence → Evidence → Evidence.
    intros a b;autoev.
    exists (λ- (fun z => pair (a@z) (b@z))).
    apply (beta_sep (fun y=>λ- (λ z : X, pair (a @ z) (y @ z))) b);intuition.
    apply (beta_sep (fun x => λ-(fun y=>λ- (λ z : X, pair (x @ z) (y @ z)))) a);intuition.
    realizer (λ+ λ+ λ+ λ+ ([[$0] ([$3] $1)] ([$2] $1))).
  Defined.
  
  Definition fste : Evidence.
    exists (λ-(fun x => x @ proj1)).
    realizer (λ+ [$0] (λ+ λ+ $1)).
  Defined.
    
  Definition snde : Evidence.
    exists (λ-(fun x => x @ proj2)).
    realizer (λ+ [$0] (λ+ λ+ $0)).
  Defined.

  Lemma intro_and : forall φ φ1 φ2 e1 e2,
        e1 ⊩ φ ⇒ φ1  ->  e2 ⊩ φ ⇒ φ2  → (paire e1 e2) ⊩ φ ⇒ (and_φ φ1 φ2).
  Proof.
    intros φ φ1 φ2 (e1,S1) (e2,S2) H1 H2.
    apply adjunction,beta_red,type_pair;now apply adjunction.
  Qed.
    
  Lemma elim1_and : forall φ1 φ2 : Φ, fste ⊩ (and_φ φ1 φ2) ⇒ φ1 .
  Proof.
    intros φ1 φ2.
    now apply adjunction,beta_red,(type_proj1  _ φ2).
  Qed.    
    
  Lemma elim2_and : forall φ1 φ2 : Φ, snde ⊩ (and_φ φ1 φ2) ⇒ φ2 .
  Proof.
    intros φ1 φ2.
    now apply adjunction,beta_red,(type_proj2 φ1).
  Qed.

  (** *** Disjunction *)

  Definition or_φ := fun a b => a ⊕ b.

  Definition inle : Evidence.
    exists (λ- (fun x => inj1 x)).
    realizer (λ+ λ+ λ+ [$1] $2). 
  Defined.
  
  
  Definition inre : Evidence.
    exists (λ- (fun x => inj2 x)).
    realizer (λ+ λ+ λ+ [$0] $2). 
  Defined.
      
  Definition  matche : Evidence -> Evidence -> Evidence.
    intros (e1,S1) (e2,S2).
    exists (λ- (fun x => (x@e1)@e2)).
    apply (beta_sep (fun y => (λ- (fun x => (x@e1)@ y))) e2);intuition.
    apply (beta_sep (fun z => λ- (fun y => (λ- (fun x => (x@z)@ y)))) e1);intuition.
    realizer (λ+ λ+ λ+ [[$0] $2] $1).
  Defined.    

  Lemma intro1_or : forall φ1 φ2 : Φ, inle ⊩ φ1 ⇒ (or_φ φ1 φ2).
  Proof.
    intros φ1 φ2.
    apply adjunction,beta_red.
    now apply type_inj1.
  Qed.
    
  Lemma  intro2_or : forall φ1 φ2 : Φ, inre ⊩ φ2 ⇒ (or_φ φ1 φ2).
  Proof.
  Proof.
    intros φ1 φ2.
    apply adjunction,beta_red.
    now apply type_inj2.
  Qed.
  

  Lemma elim_or : forall φ1 φ2 φ e1 e2,
       e1 ⊩ φ1⇒ φ   ->  e2 ⊩ φ2⇒ φ   → (matche e1 e2) ⊩ (or_φ φ1 φ2) ⇒ φ.
  Proof.
    intros φ1 φ2 φ (e1,H1) (e2,H2) E1 E2.
    apply adjunction,beta_red.
    do 2 apply adjunction.
    auto_meet_elim_trans.
    apply arrow_mon;[|apply arrow_mon_l];assumption.
  Qed.    
    

  (** *** Implication *)

  Definition imp_φ : Φ -> Φ -> Φ:=
    fun x y => x ↦ y.

  (* a x b ↦ e   => a ↦ b ↦ c*)
  Definition lambdae: Evidence -> Evidence.
   intros (x,Hx).
   exists (λ- (fun x1 => λ- (fun x2 => x@(pair x1 x2)))).
   apply (beta_sep (fun x =>  λ- (λ x1 : X, λ- (λ x2 : X, x @ pair x1 x2))) x);intuition.
   realizer (λ+ λ+ λ+ ([$2] (λ+ [[$0] $2] $1 ))).
  Defined.      

  (* ((a↦b)xa)↦ b*)
  Definition evale : Evidence.
    exists (λ- (fun x => x@(λ- (fun y => λ- (fun z => y@z))))).
    realizer (λ+ [$0] λ+ λ+ [$1] $0).
  Defined.

  
  Lemma intro_imp : forall φ φ1 φ2 : Φ, forall e : Evidence,
        e ⊩ (and_φ φ φ1) ⇒ φ2 -> lambdae e ⊩ φ ⇒ (imp_φ φ1 φ2).
  Proof.
    intros φ φ1 φ2 (e,S) H.
    apply adjunction,beta_red.
    auto_meet_elim_trans.
    apply arrow_mon_r.
    apply adjunction.
    transitivity (and_φ φ φ1 ↦ φ2).
    exact H.
    now apply arrow_mon_l,type_pair.
  Qed.
  
  Lemma elim_imp : forall φ1 φ2 : Φ,
      evale ⊩ (and_φ (imp_φ φ1 φ2) φ1) ⇒ φ2.
  Proof.
    intros φ1 φ2.
    apply adjunction,beta_red,adjunction.
    auto_meet_elim_trans.
    apply arrow_mon_l.
    do 2 (apply adjunction,beta_red).
    now apply adjunction.
  Qed.


  (** *** Forall *)
  Definition forall_φ :forall (I : Set), (I -> Φ) -> Φ:=
    fun I f => [∀] f.
  Definition fai : Evidence -> Evidence:= fun x => x.
  Definition fae : Evidence:= id.

  Lemma intro_fa : forall (I : Set),forall (φ : Φ) (φi : I -> Φ), forall e : Evidence,
          (forall i : I, e ⊩ φ ⇒ (φi i)) -> fai e ⊩ φ ⇒ ([∀] φi).
  Proof.
    intros I φ φi e H.
    unfold fai,realizes.
    apply adjunction.
    apply type_fa_intro.
    intros; apply adjunction,(H i).
  Qed.
  
    
  Lemma elim_fa:forall (I : Set), forall φi : I -> Φ, forall i : I,
          fae ⊩ [∀] φi ⇒ (φi i).
  Proof.
    intros I φi i.
    apply adjunction,beta_red.
    apply meet_fam_elim.
  Qed.


  (** *** Exists *)  
  Definition exists_φ : forall (I : Set), (I -> Φ) -> Φ:=
    fun I f => [∃] f.
  
  Definition exi : Evidence.
    exists (λ-(fun y=> λ- (fun x => x@y))).
    realizer (λ+ λ+ [$O] $1).
  Defined.

  Definition exe : Evidence -> Evidence.
    intros (e,S).
    exists (λ- (λ y : X, y @ (λ- (λ x : X, e @ x)))).
    apply~ (beta_sep (fun e=>   λ- (λ y : X, y @ (λ- (λ x : X, e @ x)))) e).
    realizer (λ+ λ+ [$0] (λ+ [$2] $0)).
  Defined.
    
  Lemma intro_ex : forall (I : Set), forall φi : I -> Φ, forall i : I,
          exi ⊩ φi i ⇒ [∃] φi.
  Proof.
      intros.
      apply adjunction,beta_red, type_ex_intro.
      exists i; intuition.
    Qed.  
     
      
  Lemma elim_ex : forall (I : Set), forall (φi : I -> Φ) (φ : Φ), forall e : Evidence,
          (forall i : I, e ⊩ φi i ⇒ φ) -> exe e ⊩ [∃] φi ⇒ φ.
  Proof.
    intros I φi φ (e,S) H.
    apply adjunction,beta_red.
    eapply type_ex_elim;[apply by_refl;reflexivity| ].
    intros i x Hi.
    transitivity (e @ (φi i)).
    apply app_mon_r,Hi.
    apply adjunction,(H i).
  Qed.

  

  (** This indeed defines an evidenced frame. *)
  
  Global Instance IA_EF : @EvidencedFrame Φ Evidence realizes :=
    {
     eid:=id; id_ax:=ax_id;
     trans:=comp; trans_ax:=ax_comp;
     φ_top:= top_φ; etrue:=tt; true_ax:=ax_true;
     φ_bot:= bot_φ; efalse:=ff; false_ax:=ax_false;
     φ_and:= and_φ; epair:=paire;efst:=fste;esnd:=snde;
     and_intro:=intro_and;and_elim1:=elim1_and;and_elim2:=elim2_and;
     φ_or:=or_φ; einl:=inle; einr:=inre; ematch:= matche;
     or_intro1:=intro1_or; or_intro2:=intro2_or; or_elim:=elim_or;
     φ_imp:=imp_φ; elambda:=lambdae ; eeval:=evale;
     imp_intro:=intro_imp; imp_elim:=elim_imp;
     φ_forall:=forall_φ; e_fai:=fai; e_fae:= fae;
     forall_intro:=intro_fa; forall_elim:=elim_fa;
     φ_exists:=exists_φ; e_exi:= exi; e_exe:=exe;
     exists_intro:=intro_ex; exists_elim:= elim_ex
    }.

  
  Definition Fork:= ⋂[a] ⋂[b] (a ↦ b ↦ meet a b).
  Notation "⋔":=Fork.

   
  Lemma meet_fam2:forall φ1 φ2,
      φ_forall (fam2 φ1 φ2) = meet φ1 φ2.
  Proof.
    intros.
    unfold φ_forall. simpl.
    unfold forall_φ,forallt,meet_family.
    apply antisymmetry.
    - apply meet_glb;split.
      + auto_meet_elim. exists true.
        unfold fam2. now rewrite If_l.
      + auto_meet_elim. exists false.
        unfold fam2. now rewrite If_r.
    - auto_meet_intro.
      destruct a;unfold fam2;[rewrite~ If_l|rewrite~ If_r].
  Qed.        
  
  Definition emeaningless `{H: ⋔∈ʆ}:Evidence.
    exists (λ- (fun x => x@⋔)).
    apply~ (beta_sep (fun y=> (λ- (fun x => x@y))) ⋔).
    realizer (λ+ λ+ [$0] $1).
  Defined.    
    
  Lemma fork_meaningless: ⋔∈ʆ -> meaningless.
  Proof.
    intros H.
    exists (@emeaningless H).
    intros φ1 φ2. rewrite meet_fam2.
    apply adjunction,beta_red,adjunction.
   auto_meet_elim_trans.
    apply arrow_mon_l.
    auto_meet_elim_trans.
  Qed.


  
  Lemma meaningless_fork: meaningless -> ⋔∈ʆ.
  Proof.
    intros ((e,S),H).
    apply up_closed with
    (λ- (fun x => λ- (fun y => e@(pair x y)))).
    - repeat auto_meet_intro.
      do 2 apply adjunction,beta_red.
      apply adjunction.
      specialize (H a a0);rewrite meet_fam2 in H.
      transitivity (φ_and a a0 ↦ (a ⋀ a0)).
      exact H.
      apply arrow_mon_l.
      now apply type_pair.
    - apply~ (beta_sep (fun e=>   λ- (λ x : X, λ- (λ y : X, e @ pair x y))) e).
      realizer (λ+ λ+ λ+ [$2] (λ+ [[$0] $2] $1)).
  Qed.


  Lemma ef_consistent: consistent <-> not(⊥∈ʆ).
  Proof.
    split;intros H1 H2.
    - apply H1. unfold φ_bot.
      simpl. unfold bot_φ.
      exists (exist (λ x : X, x ∈ʆ) (K @ ⊥) (app_closed K ⊥ sep_K H2)).
      apply adjunction.
      auto_meet_elim_trans.
    - apply H1.
      destruct H2 as ((e,He),H).
      apply up_closed with (e@⊤).
      apply adjunction,H.
      apply app_closed;intuition.
      apply up_closed with e;intuition.
  Qed.
  
End IA_to_EF.

    

  (** * Induced implicative structure *)


  Section EF_to_IA.
    Context `{Φ:Type}.
    Context `{Evidence:Type}.
    Context `{realize:Evidence -> Φ -> Φ -> Prop}.
    Context `{EF:@EvidencedFrame Φ Evidence realize}.
    
    Definition EF_arrow (A B:@Ens Evidence) := fun (r:Evidence) => ( ∀a,  A a  → B (trans r a)).
    Definition carreer:@Ens Evidence:=fun x=> True.                                             
    
    #[refine] Global Instance EF_IS: 
      @ImplicativeStructure _ _ _ (@Powerset_CompleteLattice _ carreer):= {
        arrow := EF_arrow
      }.
    Proof.
      - intros A A' B H e real.  unfold In in *. intros a Ha. apply real,H. trivial.
      - intros A A' B H e real.  intros b Hb. apply H,real. trivial.
      - intros A B.  apply Extensionality_Ensembles.
        split; intros e He;unfold In, EF_arrow in *.
        + intros E  [s [Hs HE]]. rewrite HE; simpl.
          intros a Ha. eapply Intersection_set_lb;[ | apply He];trivial.
        + intros a Ha S HS.
          unfold arrow_set_r,meet_set,Powerset_CompleteLattice,Intersection_set in He.
          specialize (He (λ e , ∀ a , A a → S (trans e a))).
          apply He;trivial.
          exists S;split;trivial. 
    Defined.

    Global Instance EF_IA: @ImplicativeAlgebra _ _ _ _ EF_IS:=IS_IA.

    
    End EF_to_IA.

