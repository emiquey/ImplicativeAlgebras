# Project Title

Coq Formalization of Implicative Algebras. The source code can be browsed at https://emiquey.gitlab.io/ImplicativeAlgebras/.

### Prerequisites

This development compiles under Coq 8.8 with Charguéraud TLC library.
Coq can be installed through opam (which is probably itself in your favorite distribution repositories):
```
opam init -n -j 2 # 2 is the number of CPU cores
opam repo add coq-released http://coq.inria.fr/opam/released
opam install coq.8.8.2 && opam pin add coq 8.8.2
```
You might be wanting to install coqide too:
```
opam install coqide
```

The [TLC] (http://www.chargueraud.org/softs/tlc/) library can be installed through the following procedure:
```
git clone https://gitlab.inria.fr/charguer/tlc.git
cd tlc
make
make install
```

### Installing

To compile the files, a simple call to the Makefile:
```
make
```
should do the job.
Then you can browse any of the file with CoqIDE or you favorite IDE.

## Authors

* **Étienne Miquey**

## Acknowledgments

* I am very grateful for all the help I got from Théo Zimmerman in Paris and from the different coqueux in the office next door in Nantes.

