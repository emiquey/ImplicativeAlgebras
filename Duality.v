Set Implicit Arguments.
Set Contextual Implicit.

Require Import Utf8.
Require Import Setoid.
Require Import Sets.Ensembles.
Require Import ImpAlg.Lattices.
Require Export ImpAlg.ImplicativeStructures.
Require Export ImpAlg.ParAlgebras.
Require Export ImpAlg.TensorAlgebras.



(** *    From  ⅋-structure to ⊗-structure     *)

(** We show that starting from a disjunctive structure,
the structure based on the reversed underlying complete lattice 
with the same negation and a⊗b := a⅋b  defines a conjunctive structure. *)

Section ReversePS.
  Context `{PS:ParStructure}.

  
  Definition tens a b:=par a b.
  
  Ltac autorev:=intros;unfold tens,rev,reverse;auto.
  
 
 Lemma rev_tensor_mon: ∀ a a' b b', a ◃ a' -> b ◃ b' ->  tens a  b ◃ tens a'  b'.
  Proof.
    autorev.
  Qed.
    
  Lemma rev_tneg_mon: ∀ a a', a ◃ a' -> (pneg a') ◃ (pneg a).
  Proof.
    autorev.
  Qed.

  Lemma rev_tensor_join_l: ∀ A b, tens (meet_set A) b = meet_set (@tensor_set_l X tens A b).
  Proof.
    unfold tensor_set_l.
    apply par_meet_l.
  Qed.
  
  Lemma rev_tensor_join_r: ∀ a B, tens a (meet_set B) = meet_set (@tensor_set_r X tens a B).
  Proof.
    unfold tensor_set_l.
    apply par_meet_r.
  Qed.

  Lemma rev_tneg_join: ∀ A, pneg (meet_set A) = join_set (image_set pneg A).
  Proof.
    apply pneg_meet.
  Qed.  
End ReversePS.


Global Instance PS_TS `{PS:ParStructure}:@TensorStructure _ _ _ RevCompleteLattice:=
  {tensor:=par; tneg:= pneg; tensor_mon:=rev_tensor_mon;
   tneg_mon:=rev_tneg_mon;tensor_join_l:=rev_tensor_join_l;
   tensor_join_r:=rev_tensor_join_r; tneg_join:=rev_tneg_join}.



(** **    Reversing a ⊗-structure          *)

(** Conversly, we show that strating from a conjunctive structure,
the structure based on the reversed underlying complete lattice 
with the same negation and a⅋b := a⊗b  defines a disjunctive structure. *)


Section ReverseTS.
  Context `{TS:TensorStructure}.

  Notation "¬ a":=(tneg a).
  
  Ltac autorev:=intros;unfold rev,reverse;auto.

  Lemma rev_par_mon: ∀ a a' b b', a ◃ a' -> b ◃ b' -> tensor a  b ◃ tensor a'  b'.
  Proof.
    autorev.
  Qed.
    
  Lemma rev_pneg_mon: ∀ a a', a ◃ a' -> (¬ a') ◃ (¬ a).
  Proof.
    autorev.
  Qed.

  Lemma rev_par_meet_l: ∀ A b, tensor (join_set A) b = join_set (@par_set_l X tensor A b).
  Proof.
    unfold par_set_l. 
    apply tensor_join_l.
  Qed.
  
  Lemma rev_par_meet_r: ∀ a B, tensor a  (join_set B) = join_set (@par_set_r X tensor a B).
  Proof.
    unfold par_set_l. 
    apply tensor_join_r.
  Qed.

  Lemma rev_pneg_meet: ∀ A, tneg (join_set A) = meet_set (image_set tneg A).
  Proof.
    apply tneg_join.
  Qed.

End ReverseTS.


Global Instance TS_PS `{TS:TensorStructure}:@ParStructure _ _ _ RevCompleteLattice:=
  {par:=tensor; pneg:=tneg; par_mon:=rev_par_mon;
   pneg_mon:=rev_pneg_mon; par_meet_l:=rev_par_meet_l; par_meet_r:=rev_par_meet_r;
   pneg_meet:=rev_pneg_meet}.




Section ReverseTensAlg.
  Context `{TA:TensorAlgebra}.

  Notation "¬ a":=(tneg a).
  Definition pre_tneg := preimage (fun a => tneg a).
  Definition pre_tneg_set:= preimage_set (fun a => ¬ a). 
  Notation "¬⁻¹ a":=(pre_tneg a) (at level 75):ia_scope.
  Notation "a ◃ b":= (rev Ord a b) (at level 70):ia_scope.
  
  
  
  Definition PS:=TS_PS.
  Definition revtsep := pre_tneg_set (tsep).
  
  Notation "a ⅋ b":= (@par _ _ _ _ PS a b).
  
  Lemma in_pre_tneg : ∀ A a, pre_tneg_set A a ↔  A (¬a).
  Proof.
    intros;split;intro H.
    - destruct H as (b,(Ab,Hb));subst;intuition.
    - exists (¬ a); auto.
  Qed.  
  
  Lemma in_revtsep : ∀ a, revtsep a ↔ tsep (¬a).
  Proof.
    intro a. unfold revtsep.
    rewrite in_pre_tneg.
    intuition.
  Qed.
  
  Lemma up_closed_psep: ∀ a b, a ◃ b → revtsep a → revtsep b.
  Proof.
    intros a b Hab Ha.
    rewrite in_revtsep;rewrite in_revtsep in Ha.
    apply tup_closed with (¬ a);intuition.
  Qed.
  
  Definition parr:=(@parrow _ _ _ _ PS).
  Infix "⅋↦":=(parr) (at level 60, right associativity). 
  
  Ltac TAstep ax:= eapply tup_closed;[|apply ax]; repeat auto_meet_intro;  auto_meet_elim_trans.
  
  
  (** The key lemma, which states that an arrow  a ⊗↦ b is in the conjunctive separator
      if and only if ¬a ⅋↦ ¬b is in the disjunctive separator. *)

      
  Lemma arrow_psep_tsep: ∀ a b, (revtsep ( a ⅋↦ b) ↔ tsep ((¬a) ⊗↦ (¬b))).
  Proof.
    intros a b ;split;intro H.
    apply (contraposition b).
    - unfold tarrow. apply tens_comm_sep.
      destruct H as (c,(H,Hc));subst.
      exact H.
    - apply (contraposition b),tens_comm_sep in H.
      later;split;try reflexivity.
      exact H.
  Qed.
  
  
  Lemma modus_ponens_revtsep: ∀ a b, revtsep a → revtsep (a ⅋↦ b) → revtsep b.
  Proof.
    intros a b Ha Hab.
    rewrite in_revtsep.
    rewrite in_revtsep in Ha.
    rewrite arrow_psep_tsep in Hab.
    apply contraposition,tens_comm_sep in Hab.
    eapply tmodus_ponens;[exact Ha|exact Hab].
  Qed.




  (** We prove that we have the necessary axioms in the separator. *)
 
  Lemma revtsep_PS1: revtsep (@PS1 _ _ _ _ PS).
  Proof.
    unfold PS1,revtsep,pre_tneg_set,preimage_set, meet_set,RevCompleteLattice.
    eexists;split;try reflexivity.
    rewrite tneg_join.
    apply tup_closed with ( ⋂[ a] ¬(¬ (a ⊗ a) ⊗ a)).
    - auto_meet_intro. intros c (z,((a,Ha),H));subst.
      apply meet_elim.
      eexists;split;try reflexivity.
    - exact tsep_TS1.
  Qed.
  
  Lemma revtsep_PS2: revtsep (@PS2 _ _ _ _ PS).
  Proof.
    unfold PS2,revtsep,pre_tneg_set,preimage_set, meet_set,RevCompleteLattice.
    eexists;split;try reflexivity.
    rewrite tneg_join.
    apply tup_closed with (TS2);intuition.
    auto_meet_intro;intros c (z,((a,Ha),H));subst; rewrite tneg_join.
    auto_meet_intro;intros c (z,((b,Hb),H));subst.
    auto_meet_elim_trans.
    apply tneg_mon.
    repeat (apply tensor_mon); try reflexivity.
    now apply tneg_mon.
Qed.
    
  
  Lemma revtsep_PS3: revtsep (@PS3 _ _ _ _ PS).
  Proof.
    unfold PS3,revtsep,pre_tneg_set,preimage_set, meet_set,RevCompleteLattice.
    eexists;split;try reflexivity.
    rewrite tneg_join.
    apply tup_closed with (TS3);intuition.
    auto_meet_intro;intros c (z,((a,Ha),H));subst; rewrite tneg_join.
    auto_meet_intro;intros c (z,((b,Hb),H));subst.
    auto_meet_elim_trans.
    apply tneg_mon.
    repeat (apply tensor_mon); try reflexivity.
    now apply tneg_mon.
  Qed.


  Lemma revtsep_PS4: revtsep (@PS4 _ _ _ _ PS).
  Proof.
    unfold PS4,revtsep,pre_tneg_set,preimage_set, meet_set,RevCompleteLattice.
    eexists;split;try reflexivity.
    rewrite tneg_join.
    apply tup_closed with (TS4);intuition.
    auto_meet_intro;intros c (z,((a,Ha),H));subst; rewrite tneg_join.
    auto_meet_intro;intros c (z,((b,Hb),H));subst; rewrite tneg_join.
    auto_meet_intro;intros d (z,((c,Hc),H));subst.
    auto_meet_elim_trans.
    repeat (apply tneg_mon;repeat (apply tensor_mon); try reflexivity).
  Qed.

  Lemma revtsep_PS5: revtsep (@PS5 _ _ _ _ PS).
  Proof.
    unfold PS5,revtsep,pre_tneg_set,preimage_set, meet_set,RevCompleteLattice.
    eexists;split;try reflexivity.
    rewrite tneg_join.
    apply tup_closed with (TS5);intuition.
    auto_meet_intro;intros c (z,((a,Ha),H));subst; rewrite tneg_join.
    auto_meet_intro;intros c (z,((b,Hb),H));subst; rewrite tneg_join.
    auto_meet_intro;intros d (z,((c,Hc),H));subst.
    auto_meet_elim_trans.
    repeat (apply tneg_mon;repeat (apply tensor_mon); try reflexivity).
  Qed.

  


Global Instance TA_PA:@ParAlgebra _ _ _ _ PS:=
  {psep:=revtsep;pmodus_ponens:=modus_ponens_revtsep;
   pup_closed:= up_closed_psep; psep_PS1:=revtsep_PS1;
   psep_PS2:=revtsep_PS2;psep_PS3:=revtsep_PS3;psep_PS4:=revtsep_PS4;psep_PS5:=revtsep_PS5}.
End ReverseTensAlg.



(** ** From disjunctive to conjunctive algebras *)


(** We prove that from any disjunctive algebra can be construct a
    conjuctive algebra by duality. The conjunctive separator is 
    defined as the preimage through the negation of the
    disjunctive separator. *)


Section ReverseParAlg.
  Context `{PA:ParAlgebra}.

  Notation "¬ a":=(pneg a).
  Definition pre_pneg := preimage (fun a => pneg a).
  Definition pre_pneg_set:= preimage_set (fun a => ¬ a). 
  Notation "¬⁻¹ a":=(pre_pneg a) (at level 75):ia_scope.
  Notation "a ◃ b":= (rev Ord a b) (at level 70):ia_scope.
  
  
  
  Definition TS:=PS_TS.
  Definition revsep := pre_pneg_set (psep).
  
  Notation "a ⊗ b":= (@tensor _ _ _ _ TS a b).
  
  Lemma in_pre_pneg : ∀ A a, pre_pneg_set A a ↔  A (¬a).
  Proof.
    intros;split;intro H.
    - destruct H as (b,(Ab,Hb));subst;intuition.
    - exists (¬ a); auto.
  Qed.  
  
  Lemma in_revsep : ∀ a, revsep a ↔ psep (¬a).
  Proof.
    intro a;unfold revsep.
    rewrite in_pre_pneg.
    intuition.
  Qed.
  
  Lemma up_closed_tsep: ∀ a b, a ◃ b → revsep a → revsep b.
  Proof.
    intros a b Hab Ha.
    rewrite in_revsep;rewrite in_revsep in Ha.
    apply pup_closed with (¬ a);intuition.
  Qed.
  
  Definition tarr:=(@tarrow _ _ _ _ TS).
  Infix "⊗↦":=(tarr) (at level 60, right associativity). 
  
  Ltac PAstep ax:= eapply pup_closed;[|apply ax]; repeat auto_meet_intro;  auto_meet_elim_trans.
  
  
  (** The key lemma, which states that an arrow  a ⊗↦ b is in the conjunctive separator
      if and only if ¬a ⅋↦ ¬b is in the disjunctive separator. 
      It is also central in the proof that the induced triposes are isomorphic.
      *)

      
  Lemma arrow_tsep_psep: ∀ a b, (revsep ( a ⊗↦ b) ↔ psep ((¬a) ⅋↦ (¬b))).
  Proof.
    intros a b ;split;intro H.
    - eapply pmodus_ponens with ((a⅋ ¬b)).
      + eapply pmodus_ponens with (¬¬(a⅋ ¬b)).
        * apply in_revsep;assumption.
        * apply dne_entails. 
      + eapply pmodus_ponens;[exact (dni_entails a)|].
        eapply pup_closed;[|apply psep_PS4_r].
        apply (meet_elim_trans);later.
        split;[exists a;reflexivity|].
        apply (meet_elim_trans);later.
        split;[exists (¬¬a);reflexivity|].
        apply (meet_elim);exists  (¬b).
        reflexivity.
    -  apply in_revsep.
       eapply pmodus_ponens with ((a⅋ ¬b)).
       + eapply pmodus_ponens with ((¬¬a)⅋ ¬b).
         * assumption.
         * eapply pmodus_ponens;[exact (dne_entails a)|].
           eapply pup_closed;[|apply psep_PS4_r].
           apply (meet_elim_trans);later.
           split;[exists (¬¬a);reflexivity|].
           apply (meet_elim_trans);later.
           split;[exists (a);reflexivity|].
           apply (meet_elim);exists  (¬b).
           reflexivity.
       + apply dni_entails.
  Qed.
  
  
  Lemma modus_ponens_revsep: ∀ a b, revsep a → revsep (a ⊗↦ b) → revsep b.
  Proof.
    intros a b Ha Hab.
    rewrite in_revsep.
    rewrite in_revsep in Ha.
    rewrite arrow_tsep_psep in Hab.
    eapply pmodus_ponens;[exact Ha|exact Hab].
  Qed.

  Lemma tmodus_ponens_revsep: ∀ a b, revsep a → revsep (¬(a ⊗ b)) → revsep (¬b).
  Proof.
    intros a b Ha Hab.
    rewrite in_revsep.
    rewrite in_revsep in Ha.
    rewrite in_revsep in Hab.
    eapply pmodus_ponens;[|apply dni_entails].
    apply par_neg_modpon with a;auto.
    eapply pmodus_ponens;[|apply dne_entails].
    assumption.
  Qed.

  Lemma tpair_revsep: ∀ a b, revsep a → revsep b → revsep (a⊗b).
  Proof.
    intros a b Ha Hb.
    rewrite in_revsep.
    rewrite in_revsep in Ha.
    rewrite in_revsep in Hb.
    eapply pmodus_ponens;[|apply imp_bot_pneg].
    apply C18_p;(eapply pmodus_ponens;[|apply pneg_imp_bot]);assumption.
  Qed.


 
  (** We prove that we have the necessary axioms in the separator. *)
 
  Lemma revsep_TS1: revsep (@TS1 _ _ _ _ TS).
  Proof.
    unfold TS1,revsep,pre_pneg_set,preimage_set,meet_set,RevCompleteLattice.
    eexists;split;try reflexivity.
    apply pup_closed with ((¬ ¬ ⋂[ a](¬ (a ⊗ a) ⊗ a))).
    - apply pneg_mon.
      rewrite pneg_meet.
      unfold image_set.
      auto_join_intro.
      apply join_elim.
      eexists;split;try reflexivity.
      eexists;split;try reflexivity.
    - eapply pmodus_ponens;[|apply dni_entails].
      exact psep_PS1.
  Qed.
  
  Lemma revsep_TS2: revsep (@TS2 _ _ _ _ TS).
  Proof.
    unfold TS2,revsep,pre_pneg_set,preimage_set,meet_set,RevCompleteLattice.
    eexists;split;try reflexivity.
    apply pup_closed with ((¬ ¬ ⋂[ a] ⋂[b] (¬a ⊗ (a ⊗ b)))).
    - apply pneg_mon.
      rewrite pneg_meet.
      unfold image_set.
      auto_join_intro.
      auto_join_intro.
      apply join_elim_trans.
      eexists;split.
      eexists;split;try reflexivity.
      eexists;split;try reflexivity.
      rewrite pneg_meet.
      unfold image_set.
      apply join_elim.
      eexists;split;try reflexivity.
      eexists;split;try reflexivity.

    - eapply pmodus_ponens;[|apply dni_entails].
      exact psep_PS2.
Qed.
    

  Lemma revsep_TS3: revsep (@TS3 _ _ _ _ TS).
  Proof.
    unfold TS3,revsep,pre_pneg_set,preimage_set,meet_set,RevCompleteLattice.
    eexists;split;try reflexivity.
    apply pup_closed with ((¬ ¬ ⋂[ a] ⋂[b] (¬(a⊗b) ⊗ (b ⊗ a)))).
    - apply pneg_mon.
      rewrite pneg_meet.
      unfold image_set.
      auto_join_intro.
      auto_join_intro.
      apply join_elim_trans.
      eexists;split.
      eexists;split;try reflexivity.
      eexists;split;try reflexivity.
      rewrite pneg_meet.
      unfold image_set.
      apply join_elim.
      eexists;split;try reflexivity.
      eexists;split;try reflexivity.
    - eapply pmodus_ponens;[|apply dni_entails].
      exact psep_PS3.
Qed.


    
  Lemma revsep_TS4: revsep (@TS4 _ _ _ _ TS).
  Proof.
    unfold TS4,revsep,pre_pneg_set,preimage_set,meet_set,RevCompleteLattice.
    eexists;split;try reflexivity.
    apply pup_closed with ((¬ ¬ ⋂[ a] ⋂[b] ⋂[c] (¬ (¬ a ⊗ b) ⊗ ¬ (c ⊗ a) ⊗ c ⊗ b))).
    - apply pneg_mon.
      rewrite pneg_meet.
      unfold image_set.
      auto_join_intro.
      auto_join_intro.
      auto_join_intro.
      apply join_elim_trans.
      eexists;split.
      eexists;split;try reflexivity.
      eexists;split;try reflexivity.
      rewrite pneg_meet.
      unfold image_set.
      apply join_elim_trans.
      eexists;split.
      eexists;split;try reflexivity.
      eexists;split;try reflexivity.
      rewrite pneg_meet.
      unfold image_set.
      apply join_elim.
      eexists;split;try reflexivity.
      eexists;split;try reflexivity.
    - eapply pmodus_ponens;[|apply dni_entails].
      exact psep_PS4.
Qed.

  Lemma revsep_TS5: revsep (@TS5 _ _ _ _ TS).
  Proof.
    unfold TS5,revsep,pre_pneg_set,preimage_set,meet_set,RevCompleteLattice.
    eexists;split;try reflexivity.
    apply pup_closed with ((¬ ¬ ⋂[ a] ⋂[b] ⋂[c] (¬ (a ⊗ b ⊗  c) ⊗ ((a ⊗ b) ⊗ c)))).
    - apply pneg_mon.
      rewrite pneg_meet.
      unfold image_set.
      auto_join_intro.
      auto_join_intro.
      auto_join_intro.
      apply join_elim_trans.
      eexists;split.
      eexists;split;try reflexivity.
      eexists;split;try reflexivity.
      rewrite pneg_meet.
      unfold image_set.
      apply join_elim_trans.
      eexists;split.
      eexists;split;try reflexivity.
      eexists;split;try reflexivity.
      rewrite pneg_meet.
      unfold image_set.
      apply join_elim.
      eexists;split;try reflexivity.
      eexists;split;try reflexivity.
    - eapply pmodus_ponens;[|apply dni_entails].
      exact psep_PS5.
  Qed.

  Lemma rev_dne: ∀a, revsep (¬¬ a) → revsep a.
  Proof.
    intros a (c,(Hc,H));subst.
    later;split;try reflexivity.
    eapply pmodus_ponens;try (exact Hc).
    step dne.
  Qed.

  
Global Instance PA_TA:@TensorAlgebra _ _ _ _ TS:=
  {tsep:=revsep;tpair:=tpair_revsep;tmodus_ponens:=tmodus_ponens_revsep;
   tup_closed:= up_closed_tsep; tsep_TS1:=revsep_TS1;
   tsep_TS2:=revsep_TS2;tsep_TS3:=revsep_TS3;tsep_TS4:=revsep_TS4;tsep_TS5:=revsep_TS5}.
   
Global Instance PA_KTA:@KTensorAlgebra _ _ _ _ TS:=
  {TA:=PA_TA; tdne:=rev_dne}.
 
   
   
   

End ReverseParAlg.





























