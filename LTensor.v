Set Implicit Arguments.
Set Contextual Implicit.

Require Import Utf8.
Require Import RelationClasses.
Require Import Logic.
Require Import Notations.
Require Import Sets.Ensembles.
Require Import ImpAlg.Lattices.
Require Export ImpAlg.TensorAlgebras.


Section LTensor.
  Context `{TS:TensorStructure}.
  
  Definition ok c := fst c ≤ snd c.
  Global Instance ok_Reflexive : Reflexive (fun x y=> ok (x,y)):=ord_refl.
  
  Notation "c '∈(≤)'":=(ok c) (at level 60,no associativity).
  Notation "'<' t '|' u '>'":=(pair t u) (at level 60,no associativity).
  
  
  Definition cord p p':= ok p' → ok p.

  Infix "◃":=(cord) (at level 70):lt_scope.
Global Open Scope  lt_scope.

  Global Instance cord_Reflexive : Reflexive cord.
  Proof. intros x H. exact H. Qed. 
  Global Instance cord_Transitive : Transitive cord.
  Proof. intros x y z Hx Hy H. apply (Hx (Hy H)). Qed. 

  Hint Resolve cord_Reflexive cord_Transitive.
  
  Global Instance cord_preOrder: PreOrder cord.
  Proof. split;auto. Qed.

  
  
  Lemma cord_mon: ∀ t u t' u', t ≤ t' → u' ≤ u → (t, u) ◃ (t',u').
  Proof.
    intros t u t' u' Ht Hu H.
    apply (ord_trans _ _ _ Ht);simpl.
    apply (ord_trans _ _ _ H Hu).
  Qed.
  
  Lemma cord_subset: ∀ c c', (∀ a, c' a ◃ c a) → subset  (λ x:X, ∃a, x = a ∧ ok (c a)) (λ x, ∃a, x= a ∧ ok (c' a)).
  Proof.
    intros c c' H x (a,(Hx,Ha)).
    subst x.
    exists a;intuition.
    now apply H.
  Qed.
    
  Lemma cord_meet: ∀ c c', (∀ a, c' a ◃ c a) → ⊓(λ x, ∃ a, x=a ∧ ok (c' a)) ≤ ⊓(λ x, ∃a, x= a ∧ ok (c a)).
  Proof.
  intros c c' H.
  apply meet_subset.
  now apply cord_subset.
  Qed.
  
  Lemma cord_join: ∀ c c', (∀ a, c' a ◃ c a) → ⊔(λ x, ∃ a, x=a ∧ ok (c a)) ≤ ⊔(λ x, ∃a, x= a ∧ ok (c' a)).
  Proof.
  intros c c' H.
  apply join_subset.
  now apply cord_subset.
  Qed.


  (** * Encodings of positives contexts  of the L⅋-calculus *)
  Notation "¬ a":=(tneg a).
  Definition box (t:X):= ¬ t.  
  Definition pairing a b:= a ⊗ b.  
  Definition mup c:= ⊓(λ x, ∃ a, x= a ∧ ok (c a )).  
  Notation "'µ+.' c":= (mup c) (at level 60,no associativity).
  Notation "'{' t ',' u '}'":= (pairing t u) (at level 60,no associativity).
  Notation "'[' t ']'":= (box t) (at level 60,no associativity).
  
  
  (** ** Properties of the encoding of µ+ *)
   
  Lemma mup_mon: ∀ c c', (∀ a, (c a)◃ (c' a)) → µ+.c ≤ µ+.c'.
  Proof.
    intros c c' H. unfold mup.
    auto_meet_leq.
    auto_meet_elim_risky.
    now apply H.
  Qed.
  
  (** The following reflect the property of closure of the pole under anti-reduction.
      Since the ß-reduction gives:  
      
                        <µα.c|V>  →  c[V/x]
                     
      by closure under anti-reduction, if c[V/α] ∈ ⫫   then  <µα.c|V> ∈ ⫫,
      or with the notations:

                        <µα.c|V> ◃ c[V/α] 
  *)                  
  
  Lemma mup_beta: ∀ c a, (µ+.c,a) ◃ (c a)  .
  Proof.
    intros c a H.
    unfold mup.
    auto_meet_elim_risky.
  Qed.
  
  
   (** The following is the usual property of η-expansion:
                         t  →  µα.<t|α>   
      which is reflected as in implicative structures by:
                          t ≤ µα.<t|α>

  *)
  
  Lemma mup_eta: ∀ t, t ≤ µ+.(λ a, (t,a))  .
  Proof.
    intros t.
    unfold mup.
    now auto_meet_intro. 
  Qed.
  
  
  
  
  
  (** * Encodings of negative terms of the L⅋-calculus *)
  
  Definition mun c:= ⊔(λ x, ∃ a, x= a ∧ ok (c a ) ).
  Definition mu_pair c:= ⊔(λ x, ∃ a, x=⊔(λ y, ∃ b, y= (a ⊗ b) ∧ ok (c a b))).
  Definition mu_neg c:= ⊔(λ x, ∃ a, x= (¬ a) ∧ ok (c a )).
  
  Notation "'µ-.' c":= (mun c) (at level 60,no associativity).
  Notation "'µ[].' c":= (mu_neg c) (at level 60,no associativity).
  Notation "'µ{}.' c":= (mu_pair c) (at level 60,no associativity).
  
  

  (** ** Variance properties *)
  
  Lemma mun_mon: ∀ c c', (∀ a, (c a) ◃ (c' a)) → µ-.c' ≤ µ-.c.
  Proof.
    intros c c' H. unfold mun.
    auto_join_leq.
    auto_join_elim_risky.
    now apply H.
  Qed.
  
  Lemma mu_pair_mon: ∀ c c', (∀ a b, (c a b)◃ (c' a b)) → µ{}.c' ≤ µ{}.c.
  Proof.
    intros c c' H. unfold mu_pair.
    auto_join_leq.
    auto_join_elim_risky.
    now apply H.
  Qed.
  


   Lemma mu_neg_mon: ∀ c c', (∀ a, (c a)◃ (c' a)) → µ[].c' ≤ µ[].c.
  Proof.
    intros c c' H. unfold mu_neg.
    auto_join_leq.
    auto_join_elim_risky.
    now apply H.
  Qed.
  
  


  
  (** ** η-expansion *)
  
    Lemma mun_eta: ∀ e, e ≤ mun (λ a,(a,e))  .
  Proof.
    intros e.
    unfold mun.
    auto_join_elim_risky.
  Qed.


  
  Lemma mu_pair_eta: ∀ e, µ{}.(λ a b, ({a,b},e)) ≤ e.
  Proof.
    intros e.
    now repeat auto_join_intro. 
  Qed.

  Lemma mu_neg_eta: ∀ e,  µ[].(λ a, ([a],e)) ≤ e .
  Proof.
    intros e.
    unfold mu_neg.
    now auto_join_intro. 
  Qed.
  
  
   (** ** ß-reduction *)
   

  Lemma mun_beta: ∀ c t, (t,µ-.c) ◃ (c t).
  Proof.
    intros c t H.
    unfold mun.
    auto_join_elim_risky. now simpl.
  Qed.
  
  Lemma mu_pair_beta: ∀ c a b, ({a,b},µ{}.c) ◃ (c a b).
  Proof.
    intros c a b H.
    unfold mun.
    auto_join_elim_risky.
    now apply H.
    reflexivity.
  Qed.
  
  Lemma mu_neg_beta: ∀ c a, ([a],µ[].c) ◃ (c a).
  Proof.
    intros c a H.
    unfold mu_neg.
    auto_join_elim_risky.
    now apply H.
    reflexivity.
  Qed.
  
End LTensor.
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  